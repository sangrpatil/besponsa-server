#HTMLCOV_DIR ?= htmlcov

IMAGES := cogex_core

# docker
# Build base and core images 

# Docker

build-docker-base:
	docker build -t chatbot-server-besponsa-base -f docker-base .


build-images: build-docker-base
	docker build -t chatbot-server-besponsa-core -f docker-core .


build: build-images

