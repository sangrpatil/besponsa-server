from flask import Flask, render_template, request,redirect, session,url_for
from flask import Flask,jsonify,Markup, session, Response
import os
from routes.admin import admin_blueprint
from routes.bot import bot_blueprint
from routes.user import user_blueprint
from routes.validate import validate_blueprint
from routes.metrics_chinabot import metrics_blueprint
from routes.access_key import access_key_blueprint
from routes.adverse_event import adverse_event_blueprint
from routes.messages import messages_blueprint
from routes.notifications import notifications_blueprint, listen_notification, g_notifications
from routes.groups import group_blueprint
from utility.logger import logger
from utility.mongo_dao import get_collection
from flask_babel import Babel, gettext
from flask_cors import CORS
import bson
import configparser
import threading
from utility.generate_repots_using_json import generateReportsWeekly

app = Flask(__name__)
app.config['SECRET_KEY'] = 'D3l01TT3'


babel = Babel(app)
CORS(app)
# Blueprints
app.register_blueprint(admin_blueprint)
app.register_blueprint(bot_blueprint)
app.register_blueprint(user_blueprint)
app.register_blueprint(validate_blueprint)
app.register_blueprint(metrics_blueprint)
app.register_blueprint(notifications_blueprint)
app.register_blueprint(group_blueprint)
app.register_blueprint(access_key_blueprint, url_prefix='/api')
app.register_blueprint(adverse_event_blueprint)
app.register_blueprint(messages_blueprint)

if __name__ == "__main__":

    # Set the notification fields
    try:
        generateReportsWeekly()

    except Exception as e:
        print('Error while setting global notification variable = {}'.format(e))
        print(e)

    print('Current Global Notification Variable in app.py = {}'.format(g_notifications))
    #notify_thread = threading.Thread(target=listen_notification, args=[])
    #notify_thread.start()

    app_host = os.environ.get('APP_HOST') or '0.0.0.0'
    app_port = os.environ.get('APP_PORT') or '8000'
    logger.info('Application will be started at http://%s:%s',app_host,app_port)
    # app.run(host='0.0.0.0', port=8000, debug=True,threaded=True)
    app.run(host=str(app_host), port=int(app_port))

