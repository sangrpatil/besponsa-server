/* Audio recording and streaming demo by Miguel Grinberg.

   Adapted from https://webaudiodemos.appspot.com/AudioRecorder
   Copyright 2013 Chris Wilson

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

/*
Below is for Textbox and other UI elements - moved from html to here
*/
var snd = new Audio("data:audio/wav;base64," + '');
var socketio = io.connect('localhost:5003');
//var socketio = io.connect('http://10.44.96.41:5003');

socketio.on('endsignal', function(msg){
        console.log('end signal')
        console.log('endsignal2')
        $('#new-input-message').prop('disabled', true);
        $('button').prop('disabled', true);
 })

function buttonObject(button_text)
{
$('#new-input-message').val(button_text)
var e = jQuery.Event("keydown");
e.which = 13; //choose the one you want
e.keyCode = 13;
$("html").trigger(e)
console.log('triggered')
//$('#new-input-message').val('')
}

//To autoscroll to bottom
var out = document.getElementById("conversations");
// allow 1px inaccuracy by adding 1
var isScrolledToBottom = out.scrollHeight - out.clientHeight <= out.scrollTop + 1;


function adjust_scrollbar()
{
if(isScrolledToBottom)
out.scrollTop = out.scrollHeight - out.clientHeight;
}

$( document ).ready(function()
{

//var socket = io.connect('http://localhost:5000');

//Send a intro message to UI(not sent to backend)
 //intro_msg='<li class="ChatLog__entry"> <img class="ChatLog__avatar" src="static/images/Assistant.png" /> <p class="ChatLog__message"> Hello there! How can I help you today? <time class="ChatLog__timestamp">'+moment().startOf('hour').fromNow()+'</time> </p> </li>'
 //$("#conversations ul").append(intro_msg)


//On Enter button,send message
$('html').keydown(function(e){
  console.log('another console test ')
  if(e.which==13 && $("#new-input-message").val().trim().length!=0){
  //Build a message
  var time = new Date();
  time_str = time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })

            send_message= '<li class="ChatLog__entry ChatLog__entry_mine"> <img class="ChatLog__avatar" /> <p class="ChatLog__message"> '+$("#new-input-message").val()+'&nbsp;&nbsp;<time class="ChatLog__timestamp");>'+time_str+'</time> </p> </li>'
            console.log("this is our console test ")
            console.log($("#new-input-message").val().replace("'", "\\'"))
            console.log(send_message)
            //Append it to existing conversation
            $("#conversations ul").append(send_message)

             //Send message to backed
            socketio.send($("#new-input-message").val());
$('#new-input-message').prop('disabled', true);
   //Clear chat box
            $('#new-input-message').val('')
   //Adjust scroll bar
   //adjust_scrollbar()
  }
});

});

/*
Below is for Speech to text
*/

window.AudioContext = window.AudioContext || window.webkitAudioContext;

var audioContext = new AudioContext();
var audioInput = null,
    realAudioInput = null,
    inputPoint = null,
    recording = false;
var rafID = null;


function adjust_scrollbar()
{
if(isScrolledToBottom)
out.scrollTop = out.scrollHeight - out.clientHeight;
}


socketio.on('add-wavefile', function(send_message) {
    var time = new Date();
    time_str = time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
    send_message= '<li class="ChatLog__entry ChatLog__entry_mine"> <img class="ChatLog__avatar" /> <p class="ChatLog__message"> '+send_message+'&nbsp;&nbsp;<time class="ChatLog__timestamp");>'+time_str+'</time> </p> </li>'
    $("#conversations ul").append(send_message);
});

socketio.on('message', function(msg) {
	  var time = new Date();
      time_str = time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
             new_msg='<li class="ChatLog__entry"> <img class="ChatLog__avatar" src="static/images/bot_avatar.png" /> <p class="ChatLog__message"> '+msg+' <time class="ChatLog__timestamp">'+time_str+'</time> </p> </li>'
             $("#conversations ul").append(new_msg)
             $('#new-input-message').prop('disabled', false);
             adjust_scrollbar()
	});
socketio.on('interaction_message', function(msg) {

	});

socketio.on('audioTTSOutput', function(msg) {
        try{
            snd.pause()
            playTTS(msg)
        }
        catch(err){
            playTTS(msg)
        }
    });

function playTTS(msg){
    snd.src = "data:audio/wav;base64," + msg;
    snd.play();
}

function toggleRecording( e ) {
    if (e.classList.contains('recording')) {
        // stop recording
        //if they don't click again for 10 seconds, then just end recording if not clicked
        e.classList.remove('recording');
        recording = false;
        socketio.emit('end-recording');
    } else {
        // start recording
        //
        e.classList.add('recording');
        recording = true;
        socketio.emit('start-recording', {numChannels: 1, bps: 16, fps: parseInt(audioContext.sampleRate)});
    }
}

function convertToMono( input ) {
    var splitter = audioContext.createChannelSplitter(2);
    var merger = audioContext.createChannelMerger(2);

    input.connect( splitter );
    splitter.connect( merger, 0, 0 );
    splitter.connect( merger, 0, 1 );
    return merger;
}

function toggleMono() {
    if (audioInput != realAudioInput) {
        audioInput.disconnect();
        realAudioInput.disconnect();
        audioInput = realAudioInput;
    } else {
        realAudioInput.disconnect();
        audioInput = convertToMono( realAudioInput );
    }

    audioInput.connect(inputPoint);
}

function gotStream(stream) {
    inputPoint = audioContext.createGain();

    // Create an AudioNode from the stream.
    realAudioInput = audioContext.createMediaStreamSource(stream);
    audioInput = realAudioInput;

    audioInput = convertToMono( audioInput );
    audioInput.connect(inputPoint);

    scriptNode = (audioContext.createScriptProcessor || audioContext.createJavaScriptNode).call(audioContext, 1024, 1, 1);
    scriptNode.onaudioprocess = function (audioEvent) {
        if (recording) {
            input = audioEvent.inputBuffer.getChannelData(0);

            // convert float audio data to 16-bit PCM
            var buffer = new ArrayBuffer(input.length * 2)
            var output = new DataView(buffer);
            for (var i = 0, offset = 0; i < input.length; i++, offset += 2) {
                var s = Math.max(-1, Math.min(1, input[i]));
                output.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7FFF, true);
            }
            socketio.emit('write-audio', buffer);
        }
    }
    inputPoint.connect(scriptNode);
    scriptNode.connect(audioContext.destination);

    zeroGain = audioContext.createGain();
    zeroGain.gain.value = 0.0;
    inputPoint.connect( zeroGain );
    zeroGain.connect( audioContext.destination );
}

function initAudio() {
    if (!navigator.getUserMedia)
        navigator.getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
    if (!navigator.cancelAnimationFrame)
        navigator.cancelAnimationFrame = navigator.webkitCancelAnimationFrame || navigator.mozCancelAnimationFrame;
    if (!navigator.requestAnimationFrame)
        navigator.requestAnimationFrame = navigator.webkitRequestAnimationFrame || navigator.mozRequestAnimationFrame;

    navigator.getUserMedia({audio: true}, gotStream, function(e) {
        alert('Error getting audio');
        console.log(e);
    });
}

window.addEventListener('load', initAudio );

