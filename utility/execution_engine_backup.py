from flask_socketio import emit
from utility.Interaction import Interaction
from utility.mongo_dao import get_one as get
from utility.ConversationLogger import ConversationLogger
from utility.DatabaseReader import query_db
from fuzzywuzzy import fuzz
from operator import itemgetter
import re
import pandas as pd
import numpy as np
import webbrowser
import random
import os
from flask_babel import gettext
from utility.ElasticSearchExecute import SearchES
from utility.TTS import synthesize_text
from utility.context import get as get_context
from utility.context import set as set_context

"""
Global Variables
"""


# suggestion = ''
# fuzzyFlag = False
# restartFlag = False
# queryFlag = False
# codeFlag = False
# guidedFlag = False
# audioSentFlag = False
# ASRFlag = False
# results = ''
# selectColumnQ = ''
# filterColumnQ = ''
# filterValueQ = ''
# surveyFlagNum = False
# surveyFlagComm = False
# endSurveyFlag = False


"""
Has three main functions: init, execute, resume
The rest are utility functions
@init: initializes the session with an id and starts logging
@execute: executes bot tasks, i.e. displaying a message or buttons
@resume: takes user input and determines which next state it matches with. performs a fuzzy search if uncertain
"""


class ExecutionEngine():
    def __init__(self,session_id):
        self.session_id = session_id
        self.session = get_context(session_id)
        self.conv_log = ConversationLogger(session_id)

    """
    Function: executes bot actions by giving user prompts (buttons/text/query outputs)
    This will check whether the output should be text, buttons, etc
    @:param self
    @:param task_definition: this is the current state json
    """
    def execute(self, task_definition):
        self.session['audioSentFlag'] = False
        self.session['restartFlag'] = False
        self.session['guidedFlag'] = False
        self.session['endSurveyFlag'] = False

        # audioSentFlag = False
        # restartFlag = False
        # guidedFlag = False
        # endSurveyFlag = False
        #check what the information about the current state is.
        #depending on what the interaction type is for the current state is, execute accordingly
        try:
            self.conv_log.update(task_definition['state_name']) # To update the current state in status
            self.session['botName'] = task_definition['bot_name']
            interactionObj = Interaction()
            flash_message = task_definition['flash_message']
            interaction = task_definition['interaction']['type']
            dbData = task_definition['interaction']['database_details']
            #none means the user will just be shown text, and then the conversation will restart
            if interaction == 'none':
                self.noneExecute(task_definition, flash_message,dbData)
            #if it is url, then show the hyperlink to the user
            elif interaction == 'url':
                self.urlExecute(task_definition, flash_message,dbData)
            #if it is text, then show the text to the user and wait for their input
            elif interaction == 'text':
                self.textExecute(task_definition, flash_message,dbData)
            #if it is a button, give the user buttons to select from
            elif interaction == 'button':
                print('button test')
                self.buttonExecute(task_definition, flash_message, dbData, interactionObj)
        except Exception as e:
            print('Execute Method Error - ', str(e))

    """
    Function: handles user inputs, and is called from handleMessage in main.py
    When a user types in a message, it can be one of five things:
    1. It is small talk
    2. We are expecting a yes/no in response to restarting
    3. We are expecting a yes/no in response to fuzzysearch option
    4. We are expecting the user to pick an option from hybrid guided
    5. Anything else

    @:param self
    @:param message: the message that the user passes in
    @:param sessionID: the sessionID such that the elasticsearch functionality is part of the same session
    @:param ASR: a boolean for whether or not the resume function came from the text or ASR
    """
    def resume(self,message,sessionID, ASR):
        self.session['ASRFlag'] = ASR
        self.session['audioSentFlag'] = False
        self.session['audioSentFlag_ES'] = False
        self.conv_log.user_log(message, self.session['ASRFlag'], self.session['endSurveyFlag'])
        try:
            #first check if the message is small talk
            smallTalkMsg = self.smallTalk(message)
            if smallTalkMsg != 0:
                self.send_msg(smallTalkMsg)
                self.send_interaction_msg(smallTalkMsg, "text")
            #next check if we are expecting a yes/no in response to restarting
            elif self.session['restartFlag'] is True:
                return self.restartExecute(message)
            elif self.session['endSurveyFlag'] is True:
                self.surveyExecute(message)
            #next check if we are expecting a yes/no in response to fuzzy search
            elif self.session['fuzzyFlag'] is True:
                self.fuzzyExecute(message)
            #next check if we're expecting a response from a hybrid guided
            elif self.session['guidedFlag'] is True:
                self.guidedFlagExecute(message)
            #if it was none of the above, then it is a normal message
            else:
                #first determine what the state is in the conversation, and what states can lead from here
                current_state = get('status', '_id', self.session_id)['current_state']
                state_definition = get('state', 'state_name', current_state)
                action_type = state_definition['action']['type']
                self.session['botName'] = state_definition['bot_name']
                next_possible_states = state_definition['action']['next_possible_states']
                next_possible_states_json = []
                next_possible_states_names = []
                try:
                    for next_possible_state in next_possible_states:
                        next_possible_states_json.append(get('state', 'state_id', next_possible_state))
                    for json in next_possible_states_json:
                        next_possible_states_names.append(json['state_name'])
                except Exception as e:
                    next_possible_states_json = get('state', 'state_id', 1)
                    next_possible_states_names = next_possible_states_json['state_name']
                #if the action type is code, take the user's message and apply it to the code
                if action_type == 'code':
                    self.codeExecute(message, state_definition)
                #if the action if FAQ, then take the user's message and query elasticsearch with it
                elif action_type == 'FAQ':
                    self.FAQExecute(message, sessionID, self.session['ASRFlag'])
                #if the action is hybrid guided, take the user's message and check the hybrid guided process
                elif action_type == 'hybridGuided':
                    self.hybridGuidedExecute(message, next_possible_states_names)
                #if the action is query, query the user's message against a database using DAO.py
                elif action_type == 'query':
                    self.queryExecute(message, current_state)
                #if the action type is navigate, we are navigating from one branch to another in the conversation
                elif action_type == 'navigate':
                    self.navigateExecute(message, next_possible_states_names)
        except Exception as e:
            print('Resume Method Error - ',str(e))

    """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    Here are general utility functions
    """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    #send messages to the user, log it in bot log, call TTS sendAudio function
    # def send_msg(self,msg):
    #     self.conv_log.bot_log(msg, self.session['ASRFlag'], self.session['endSurveyFlag'])
    #     emit('message', msg)
    #     msg = re.sub(r'<br>',' ',msg)
    #     msg = re.sub(r'<button .*', '', msg)
    #     self.sendAudio(msg)
    #
    # def send_interaction_msg(self,msg, interaction):
    #     #self.conv_log.bot_log(msg, session['ASRFlag'], session['endSurveyFlag'])
    #     msg_data = {
    #         "interactionType":interaction,
    #         "message":msg
    #     }
    #     emit('interaction_message', msg_data)
    #     # msg = re.sub(r'<br>',' ',msg)
    #     # msg = re.sub(r'<button .*', '', msg)
    #     # self.sendAudio(msg)

    #take message and send to TTS function from TTS.synthesize_text()
    #will only do this if ASRFlag is true, meaning ASR was called, and if it is the 1st message to be sent (to avoid sending multiple messages)
    def sendAudio(self, msg):

        if self.session['audioSentFlag'] == False and self.session['ASRFlag'] == True:
            audio = synthesize_text(msg)
            emit('audioTTSOutput', audio)
            self.session['audioSentFlag'] = True

    #this end signal tells the front end to disable all buttons
    def send_endsignal(self):
        emit('endsignal','')

    #use Fuzzywuzzy to check for fuzzysearch options for Navigate options
    #if the match is > 45% similar, return as option for user to pick from
    def fuzzySearch(self, nextStates, message):
        # global fuzzyFlag
        # global botName
        # global suggestion
        options = []
        interactionObj = Interaction()
        for state in nextStates:
            stateOutput = re.sub(r'<.*>', '', state)
            options.append([fuzz.ratio(message, stateOutput), state, stateOutput])
        self.session['suggestion'] = max(options, key=itemgetter(0))[1]
        suggestionOutput = max(options, key=itemgetter(0))[2]
        ratio = max(options, key=itemgetter(0))[0]

        if ratio > (os.environ.get('FUZZY_RATIO') or 45):
            # check if task is a navigate task, if yes, check message value, see if pass
            html = interactionObj.getYesNoButtonHTML([gettext("Yes"), gettext("No")])
            self.send_msg(gettext('We couldn\'t find that. Do you mean: ') + suggestionOutput + '<br>' + html)
            self.send_interaction_msg([gettext('We couldn\'t find that. Do you mean: '), gettext("Yes"), gettext("No")], "button")
            self.session['fuzzyFlag'] = True
        else:
            self.send_msg(gettext('Sorry we don\'t have an answer for that. Could you try again?'))
            self.send_interaction_msg(gettext('Sorry we don\'t have an answer for that. Could you try again?'), "text")

    #ask the user if they would like to restart - send button and set global restartflag to True
    def restart(self):
        # global restartFlag
        # global botName
        interactionObj = Interaction()
        html = interactionObj.getYesNoButtonHTML([gettext("Yes"), gettext("No")])
        self.send_msg(gettext('Would you like to continue?') + '<br>' + html)
        self.send_interaction_msg([gettext('Would you like to continue?'),gettext("Yes"), gettext("No")], "button")
        self.session['restartFlag'] = True

    #small talk module to check if the user is asking small talk questions
    def smallTalk(self, msg):
        greetings = [gettext('hello'), gettext('hi'), gettext('what''s up'), gettext('good afternoon'), gettext('good morning'), gettext('good day'), gettext('good evening')]
        questionQuestions = [gettext('I would like to ask a question'),gettext('I want to ask you a question'), gettext('I have a doubt'), gettext('I want to ask you something'),gettext('I want to ask you questions'), gettext('I want to consult you')]
        inquiries = [gettext('how are you'), gettext('how are you doing'), gettext('how is your day')]
        greetingResponse = [gettext('Hi there! How can I help you')]
        questionQuestionResponses = [gettext('With pleasure, tell me how I can help you.')]
        inquiriesResponse = [gettext('I''m doing well! How can I help you today?'),gettext('I''m feeling great! How can I help you today?')]
        if msg.lower().strip() in greetings:
            return random.choice(greetingResponse)
        elif msg.lower().strip() in questionQuestions:
            return random.choice(questionQuestionResponses)
        else:
            return 0

    def survey(self,state):
        # global endSurveyFlag
        # global surveyFlagNum
        # global surveyFlagComm
        self.session['endSurveyFlag'] = True
        interactionObj = Interaction()
        if state ==1:
            self.session['surveyFlagNum'] = True
            options = [1,2,3,4,5]
            html = interactionObj.getYesNoButtonHTML(options)
            self.send_msg(gettext('On a scale of 1-5, how satisfied were you with your service today?') + '<br>'+html)
            self.send_interaction_msg([gettext('On a scale of 1-5, how satisfied were you with your service today?'),1,2,3,4,5], "button")
        elif state == 2:
            self.session['surveyFlagComm'] = True
            html = interactionObj.getYesNoButtonHTML([gettext("Yes"), gettext("No")])
            self.send_msg(gettext('Would you like to leave any comments?') + '<br>' + html)
            self.send_interaction_msg([gettext('Would you like to leave any comments?'), gettext("Yes"), gettext("No")],"button")

    """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    Here are the functions used in the resume function
    """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    #If restartFlag true, execute thix
    #if yes, go back to 1st task ID (start)
    #if no, then send end signal
    def restartExecute(self, message):
        # global restartFlag
        # global queryFlag
        if message.strip().lower() == gettext('yes'):
            self.session['restartFlag'] = False
            self.session['queryFlag'] = False
            next_possible_states_json = get('state', 'state_id', 1)
            next_state_name = next_possible_states_json['state_name']
            restart = get('state', 'state_name', next_state_name)
            self.execute(restart)
        elif message.strip().lower() == gettext('no'):
            self.session['restartFlag'] = False
            self.session['queryFlag'] = False
            self.send_msg(gettext('Thanks for using the Pfizer Bot!'))
            self.send_interaction_msg(gettext('Thanks for using the Pfizer Bot!'), "text")
            self.survey(1)
            #self.send_endsignal()
        else:
            interactionObj = Interaction()
            html = interactionObj.getYesNoButtonHTML([gettext("Yes"), gettext("No")])
            self.send_msg(gettext('Would you like to continue? Please choose yes or no.') + '<br>' + html)
            self.send_interaction_msg([gettext('Would you like to continue? Please choose yes or no.'), gettext("Yes"), gettext("No")],"button")
    def surveyExecute(self, message):
        # global surveyFlagNum
        # global surveyFlagComm
        # global endSurveyFlag
        if self.session['surveyFlagNum'] is True:
            if message in '12345':
                # log survey
                self.session['endSurveyFlag'] = True
                self.session['surveyFlagNum'] = False
                self.survey(2)
                pass
            else:
                self.send_msg(gettext('Please choose an option 1-5.'))
                self.send_interaction_msg(gettext('Please choose an option 1-5.'), "text")
                self.survey(1)
        elif self.session['surveyFlagComm'] is True:
            if message.strip().lower() == gettext('yes'):
                self.send_msg(gettext('Please leave your comment below:'))
                self.send_interaction_msg(gettext('Please leave your comment below:'), "text")
            elif message.strip().lower() == gettext('no'):
                self.session['endSurveyFlag'] = False
                self.session['surveyFlagComm'] = False
                self.session['surveyFlagNum'] = False
                self.send_msg(gettext('Thank you for your participation!'))
                self.send_interaction_msg(gettext('Thank you for your participation!'), "text")
                self.send_endsignal()
            else:
                self.session['endSurveyFlag'] = False
                self.session['surveyFlagComm'] = False
                self.session['surveyFlagNum'] = False
                self.send_msg(gettext('Thank you for your participation!'))
                self.send_interaction_msg(gettext('Thank you for your participation!'), "text")
                self.send_endsignal()
    #If FuzzyFlag is True, then execute this
    #check if the fuzzysearch came from a query or code
    #if not, then check if the suggestion picked comes from a next state
    #if not what they are looking for, return to start
    def fuzzyExecute(self, message):
        # global queryFlag
        # global codeFlag
        # global selectColumnQ
        # global filterColumnQ
        # global filterValueQ
        # global fuzzyFlag
        # global restartFlag
        if str(message).lower() == gettext('yes'):
            # check if the fuzzysearch came from a query
            if self.session['queryFlag'] == True:
                if len(str(self.session['selectColumnQ'])) == 0:
                    self.session['selectColumnQ'] = self.session['suggestion']
                elif len(str(self.session['filterColumnQ'])) == 0:
                    self.session['filterColumnQ'] = self.session['suggestion']
                elif len(str(self.session['filterValueQ'])) == 0:
                    self.session['filterValueQ'] = self.session['suggestion']
                    self.session['fuzzyFlag'] = False
                current_state = get('status', '_id', self.session_id)['current_state']
                state_definition = get('state', 'state_name', current_state)
                self.execute(state_definition)
                pass
            # check if the fuzzysearch came from code
            elif self.session['codeFlag'] == True:
                current_state = get('status', '_id', self.session_id)['current_state']
                state_definition = get('state', 'state_name', current_state)
                code = state_definition['action']['function_code']
                message = self.session['suggestion']  # we pass in the message to the code
                exec(code)
                self.session['restartFlag'] = True
                self.session['fuzzyFlag'] = False
                self.session['codeFlag'] = False
                self.restart()
            else:
                self.session['fuzzyFlag'] = False
                get_next_state = get('state', 'state_name', self.session['suggestion'])
                self.execute(get_next_state)
                pass
        elif message.lower() == gettext('no'):
            self.session['fuzzyFlag'] = False
            self.session['queryFlag'] = False
            self.send_msg(gettext('Sorry about that. Lets try again from the start.'))
            self.send_interaction_msg(gettext('Sorry about that. Lets try again from the start.'), "text")
            next_possible_states_json = get('state', 'state_id', 1)
            next_state_name = next_possible_states_json['state_name']
            # restart = get('state', 'state_name', next_state_name)
            restart = get('state', 'state_name', next_state_name)
            self.execute(restart)
            pass
        else:
            interactionObj = Interaction()
            html = interactionObj.getYesNoButtonHTML([gettext("Yes"), gettext("No")])
            self.send_msg(gettext('Please choose yes or no.') + '<br>' + html)
            self.send_interaction_msg([gettext('Please choose yes or no.'),gettext("Yes"), gettext("No")], "button")

    #if expecting a response from Hybrid guided, check here if it leads to a valid state
    def guidedFlagExecute(self, message):
        global guidedFlag
        get_next_state = get('state', 'state_name', message)
        if str(get_next_state) != 'None':
            self.execute(get_next_state)
            guidedFlag = False
        else:
            self.send_msg(gettext('Please choose one of the options'))
            self.send_interaction_msg(gettext('Please choose one of the options'), "text")

    #execute code hook here if action type is code
    def codeExecute(self, message, state_definition):
        # global codeFlag
        self.session['codeFlag'] = True
        found = False
        for item in self.session['results']:
            if str(message.lower()) == str(item).lower():
                finalMessage = message
                found = True
        if found == True:
            code = state_definition['action']['function_code']
            message = finalMessage  # we pass in the message to the code
            exec(code)
            next_possible_states = state_definition['action']['next_possible_states']
            next_possible_states_json = get('state', 'state_id', next_possible_states[0])
            next_state_name = next_possible_states_json['state_name']
            restart = get('state', 'state_name', next_state_name)
            self.session['codeFlag'] = False
            self.restart()
        else:
            self.fuzzySearch(self.session['results'], message)

    #if action type is free-form FAQ, query Elasticsearch with message
    def FAQExecute(self, message, sessionID, ASR):
        # global ASRFlag

        print('FAQ Executing')
        search = SearchES(sessionID)
        x = search.execute(message, self.session['botName'], sessionID, ASR)
        if x == 0:
            self.session['ASRFlag'] = False
            self.restart()
        # self.restart()
        elif x == 1:
            # fallback here
            pass
        elif type(x) is str:
            get_next_state = get('state', 'state_name', x)
            self.execute(get_next_state)

    #if action type is hybrid, execute this
    #first check if the message is a 1:1 match with taskNames
    #if it is, execute the corresponding state
    #if it is not, check message against keywords. For each keyword matching in the message, add a point
    #return taskNames with highest keyword scores for using to pick from
    def hybridGuidedExecute(self, message, next_possible_states_names):
        # global guidedFlag
        found = False
        interactionObj = Interaction()
        for state in next_possible_states_names:
            stateCompare = re.sub(r'<.*>', '', state)
            if message.strip().lower() == stateCompare.strip().lower():
                get_next_state = get('state', 'state_name', state)
                if 'None' in str(get_next_state):
                    pass
                else:
                    found = True
                    self.execute(get_next_state)
                    pass
        if found == False:  # so here instead of a fuzzysearch, do the keyword search
            stateScores = []
            finalStates = []
            possibleNonAliasStates = []
            for state in next_possible_states_names:
                score = 0
                stateinfo = get('state', 'state_name', state)
                alias = stateinfo['alias']
                if alias == 'False':
                    keywords = stateinfo['keywords']
                    keywords = re.sub(r'，', ',', keywords)
                    keys = keywords.split(',')
                    if len(keys) > 0:
                        for key in keys: #keywords lipitor, store #user: where do i store lipitor vs where do i buy lipitor
                            if key.lower().strip() in message.lower().strip():
                                score += 1
                                # if key.lower().strip() in savedKey.lower().strip() -> score+=1
                        stateScores.append(score)
                        possibleNonAliasStates.append(state)
            # pick the state names with the highest scores, return those as options
            highScore = max(stateScores)
            if highScore != 0:
                for i in np.arange(0, len(stateScores)):
                    if stateScores[i] == highScore:
                        finalStates.append(possibleNonAliasStates[i])
            if len(finalStates) != 0:
                html = interactionObj.getButtonHTML(finalStates)
                interaction_json = [gettext('Are you interested in any of these options?')]
                for state in finalStates:
                    interaction_json.append(state)
                self.send_msg(gettext('Are you interested in any of these options?') + '<br> ' + html)
                self.send_interaction_msg(interaction_json, "button")
                self.session['guidedFlag'] = True
            else:
                self.send_msg(gettext('Sorry, we may not have information for that. Can you try again?'))
                self.send_interaction_msg(gettext('Sorry, we may not have information for that. Can you try again?'), "text")
    #if action type is query, use DAO to query external database
    def queryExecute(self, message, current_state):
        # global queryFlag
        self.session['queryFlag'] = True
        found = False
        for item in self.session['results']:
            if found == True:  # skip if we've already found it
                pass
            else:
                # itemCompare = re.sub(r' ',' ', item)
                if message.strip().lower() == str(item).lower():
                    output = item
                    found = True
                    if len(self.session['selectColumnQ']) == 0:
                        self.session['selectColumnQ'] = output
                    elif len(self.session['filterColumnQ']) == 0:
                        self.session['filterColumnQ'] = output
                    elif len(self.session['filterValueQ']) == 0:
                        self.session['filterValueQ'] = output
                    current = get('state', 'state_name', current_state)
                    self.execute(current)

        if found == False:
            self.fuzzySearch(self.session['results'], message)

    #if actiontype is navigate, compare message against possible taskNames
    def navigateExecute(self, message, next_possible_states_names):
        found = False
        for state in next_possible_states_names:
            stateCompare = re.sub(r'<.*>', '', state)
            if message.strip().lower() == stateCompare.strip().lower():
                get_next_state = get('state', 'state_name', state)
                if 'None' in str(get_next_state):
                    pass
                else:
                    found = True
                    self.execute(get_next_state)
                    pass
        if found == False:
            # do Fuzzy search here
            self.fuzzySearch(next_possible_states_names, message)

    """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    Here are functions used in the Execute function
    """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    #if interaction is none, check if query database or not
    #just show text and then restart
    def noneExecute(self, task_definition, flash_message,dbData):
        # global results
        static_values = task_definition['interaction']['values']
        if task_definition['interaction']['fetch_from_db'] != "True":  # print from flashmessage if interaction = none
            try:
                currentState = task_definition['state_name']
                message = re.sub(r'<>', currentState, flash_message)
                self.send_msg(message)
                self.send_interaction_msg(message, "text")
                if pd.isnull(static_values) == [False]:
                    self.send_msg(static_values[0])
                    self.send_interaction_msg(static_values[0],"text")
            except Exception as e:
                self.send_msg(flash_message + '<br>' + static_values[0])
                self.send_interaction_msg([flash_message, static_values[0]],"textStack")
        else:  # pull from DB if interaction = none
            try:
                self.session['results'] = query_db(str(dbData['string']), str(dbData['table_name']), [dbData['select_column']],
                                   [dbData['filter_column']], ["\'" + str(dbData['filter_value']) + "\'"])
                self.send_msg(flash_message + '<br>' + str(session['results'][0]))
                self.send_interaction_msg([flash_message, static_values[0]],"textStack")
            except Exception as e:
                message = gettext('Sorry, we currently do not have access to this data. Please try again later!')
                self.send_msg(message)
                self.send_interaction_msg(message,"text")
        self.restart()

    #if interaction is url, check if query database or not
    #then display url and restart
    def urlExecute(self, task_definition, flash_message,dbData):
        static_values = task_definition['interaction']['values']
        if task_definition['interaction']['fetch_from_db'] != "True":
            currentState = task_definition['state_name']
            message = re.sub(r'<>', currentState, flash_message)
            self.send_msg(message)
            self.send_interaction_msg(message, "text")

            if pd.isnull(static_values) == [False]:
                webbrowser.open(static_values[0], new=2, autoraise=True)
                self.send_msg('<a href="' + static_values[0] + '" target="_blank" >' + gettext('Click here to go to the link') + '</a>')
                self.send_interaction_msg([gettext('Click here to go to the link'),static_values[0]],"url")
        else:
            try:
                self.session['results'] = query_db(str(dbData['string']), str(dbData['table_name']), [dbData['select_column']],
                                   [dbData['filter_column']], ["\'" + str(dbData['filter_value']) + "\'"])
                # self.send_msg(flash_message + '<br>' + str(results[0]))
                currentState = task_definition['state_name']
                message = re.sub(r'<>', currentState, flash_message)
                self.send_msg(message)
                self.send_interaction_msg(message, "text")
                webbrowser.open(self.session['results'], new=2, autoraise=True)
            except Exception as e:
                message = gettext('Sorry, we currently do not have access to this data. Please try again later!')
                self.send_msg(message)
                self.send_interaction_msg(message, "text")
        self.restart()

    #if interaction is text, then check if query databse or not
    #then send text and wait for user response
    def textExecute(self,task_definition,flash_message,dbData):
        static_values = task_definition['interaction']['values']
        if task_definition['interaction']['fetch_from_db'] != "True":
            self.send_msg(flash_message + '<br>' + static_values[0])
            self.send_interaction_msg([flash_message, static_values[0]], "textStack")
        else:
            try:
                self.session['results'] = query_db(str(dbData['string']), str(dbData['table_name']), [dbData['select_column']],
                                   [dbData['filter_column']], ["\'" + str(dbData['filter_value']) + "\'"])
                self.send_msg(flash_message + '<br>' + str(self.session['results'][0]))
                self.send_interaction_msg([flash_message, static_values[0]], "textStack")
            except Exception as e:
                message = gettext('Sorry, we currently do not have access to this data. Please try again later!')
                self.send_msg(message)
                self.send_interaction_msg(message, "text")

    ### Button has a few options
    ### If fetch from DB is false, then we have a static value
    ### If fetch from DB is true, then we can have either a "code" action or a "query" action
    ### if the interaction type is a button and fetch from DB is true, then the action needs to either be "code" or "query"
    def buttonExecute(self, task_definition,flash_message,dbData, interactionObj):
        # Check if values are static or dynamic
        # global selectColumnQ
        # global filterColumnQ
        # global filterValueQ
        # global results
        # global queryFlag

        static_values = task_definition['interaction']['values']
        if task_definition['interaction']['fetch_from_db'] != "True":
            try:
                currentState = task_definition['state_name']
                message = re.sub(r'<>', currentState, flash_message)
                html = interactionObj.getButtonHTML(static_values)
                self.send_msg(message + '<br>' + html)
                button_json = [message]
                for item in static_values:
                    button_json.append(item)
                self.send_interaction_msg(button_json, "button")
            except Exception as e:
                html = interactionObj.getButtonHTML(static_values)
                message = flash_message
                button_json = [message]
                for item in static_values:
                    button_json.append(item)
                self.send_msg(message + '<br>' + html)
                self.send_interaction_msg(button_json, "button")

        elif task_definition['interaction']['fetch_from_db'] == "True" and task_definition['action']['type'] == "query":
            try:
                # if the select column is null, we need that
                if len(str(self.session['selectColumnQ'])) == 0:
                    if pd.isnull(dbData['select_column']) == True:
                        self.session['results'] = query_db(str(dbData['string']), str(dbData['table_name']), '')
                        # results = re.sub(r'_', ' ', results)
                        html = interactionObj.getButtonHTML(session['results'])
                        flash_message = gettext('Which category are you interested in?')
                        self.send_msg(flash_message + '<br>' + html)
                # then we need the filter column
                elif len(str(self.session['filterColumnQ'])) == 0:
                    if pd.isnull(dbData['filter_column']) == True:
                        self.session['results'] = query_db(str(dbData['string']), str(dbData['table_name']), '')
                        # results = re.sub(r'_', ' ', results)
                        html = interactionObj.getButtonHTML(session['results'])
                        flash_message = gettext('Which category would you like to filter on?')
                        self.send_msg(flash_message + '<br>' + html)
                elif len(str(self.session['filterValueQ'])) == 0:
                    if pd.isnull(dbData['filter_value']) == True:
                        self.session['results'] = query_db(str(dbData['string']), str(dbData['table_name']), [str(self.session['filterColumnQ'])])
                        # results = re.sub(r'_', ' ', results)
                        html = interactionObj.getButtonHTML(self.session['results'])
                        flash_message = gettext('Which of these do you want to know about?')
                        self.send_msg(flash_message + '<br>' + html)
                else:
                    self.session['results'] = query_db(str(dbData['string']), str(dbData['table_name']), [str(self.session['selectColumnQ'])],
                                       [str(self.session['filterColumnQ'])], ["\'" + str(self.session['filterValueQ']) + "\'"])
                    flash_message = gettext('Here is the information regarding ') + str(self.session['selectColumnQ']) + ' for ' + str(self.session['filterValueQ'])
                    self.send_msg(flash_message + '<br>' + str(self.session['results'])[1:-1])  # return whatever comes back as the list minus the []
                    self.session['results'] = ''
                    self.session['selectColumnQ'] = ''
                    self.session['filterColumnQ'] = ''
                    self.session['filterValueQ'] = ''
                    self.session['queryFlag'] = False
                    self.restart()

                pass
            except Exception as e:
                message = gettext('Sorry we currently do not have access to this data. Please try again later!')
                self.send_msg(message)
                self.send_interaction_msg(message,"text")
                self.session['results'] = ''
                self.session['selectColumnQ'] = ''
                self.session['filterColumnQ'] = ''
                self.session['filterValueQ'] = ''
                self.session['queryFlag'] = False
                self.restart()
        elif task_definition['interaction']['fetch_from_db'] == "True" and task_definition['action']['type'] == "code":
            # DAO here to create SQLlite table
            # read in table names
            try:
                self.session['results'] = query_db(str(dbData['string']), str(dbData['table_name']), [dbData['select_column']])
                html = interactionObj.getButtonHTML(self.session['results'])
                self.send_msg(flash_message + '<br>' + html)
            except Exception as e:
                message = gettext('Sorry we currently do not have access to this data. Please try again later!')
                self.send_msg(message)