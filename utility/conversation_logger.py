from utility.mongo_dao import insert,update,get_one as get, get_by_id
import datetime
from utility.session_manager import get as get_context, modify as update_context
from flask_babel import gettext
import pytz
import bson
from utility.logger import logger
"""
Function: Log conversation from the bot and the user
user_log logs user messages
bot_log logs bot messages
update is used by both user_log and bot_log to update MongoDB
"""
class ConversationLogger():

    def __init__(self,session_id, bot_id):
        self.session_id = session_id
        print("Session ID in conversation logger: ", session_id)
        #from main import globalBotName
        try:
            if not get_by_id('status',session_id):
                insert('status',{"_id":bson.ObjectId(self.session_id),"date_created":pytz.utc.localize(datetime.datetime.utcnow()),"bot_id":bot_id}) #"has_ended":False,
                session = get_context(session_id)
                session['session_id'] = session_id
                session['session_exist'] = True
                update_context(session_id,session)
        except Exception as e:
            logger.error("Error during convo log Init:")
            logger.exception(e)

    def update(self,state):
        try:
            #logger.debug('CURRENT SESSION - ', get_by_id('status',self.session_id)['history'], type(get_by_id('status', self.session_id)['history']))
            current_state = get_by_id('status',self.session_id)['current_state']
            print("Current state: ", current_state)
            history = get_by_id('status',self.session_id)['history']
            logger.debug('STATE - ', state, ' HISTORY - ', history)
            history.append(state)
            update('status', self.session_id, {'current_state': state,
                                               'prior_state': current_state,
                                               'history':history,
                                               "date_created": pytz.utc.localize(datetime.datetime.utcnow())})
        except Exception as e:
            logger.exception(e)
            print(e)
            history_list = []
            history_list.append(state)
            update('status', self.session_id, {'current_state': state,
                                               'history':history_list,
                                               "date_created": pytz.utc.localize(datetime.datetime.utcnow())})




    def updateIntentFoundCounter(self, message,category, grv_id=None):
        current_doc = get_by_id('status', self.session_id)
        try:
            current_count = len(current_doc['intentFound'])
            current_index = current_count
            type_message_index = "intentFound." + str(current_index) + "." + "intentFound"
            time_index = "intentFound." + str(current_index) + "." + "time"
            group_index = "intentFound." + str(current_index) + "." + "group"
            time = pytz.utc.localize(datetime.datetime.utcnow())
            grv_id_row = "intentFound." + str(current_index)+ "." + "grv_id"
            update('status', self.session_id, {type_message_index: message})
            update('status', self.session_id, {time_index: time})
            update('status', self.session_id, {group_index: category})
            update('status', self.session_id, {grv_id_row: grv_id})
        except:
            current_index = 0
            type_message_index = "intentFound." + str(current_index) + "." + "intentFound"
            time_index = "intentFound." + str(current_index) + "." + "time"
            group_index = "intentFound." + str(current_index) + "." + "group"
            time = pytz.utc.localize(datetime.datetime.utcnow())  # .strftime("%Y-%m-%d %H:%M")
            grv_id_row = "intentFound." + str(current_index)+ "." + "grv_id"
            update('status', self.session_id, {type_message_index: message})
            update('status', self.session_id, {time_index: time})
            update('status', self.session_id, {group_index: category})
            update('status', self.session_id, {grv_id_row: grv_id})

    def updateIntentNotFound(self, message,category, grv_id=None):
        print('updating intent not found')

        current_doc = get_by_id('status', self.session_id)
        try:
            current_count = len(current_doc['intentNotFound'])
            current_index = current_count
            type_message_index = "intentNotFound." + str(current_index) + "." + "intentNotFound"
            time_index = "intentNotFound." + str(current_index) + "." + "time"
            group_index= "intentNotFound." + str(current_index) + "." + "group"
            time = pytz.utc.localize(datetime.datetime.utcnow())  # .strftime("%Y-%m-%d %H:%M")
            grv_id_row = "intentNotFound." + str(current_index)+ "." + "grv_id"
            update('status', self.session_id, {type_message_index: message})
            update('status', self.session_id, {time_index: time})
            update('status', self.session_id, {group_index: category})
            update('status', self.session_id, {grv_id_row: grv_id})
        except:
            current_index  = 0
            type_message_index = "intentNotFound." + str(current_index) + "." + "intentNotFound"
            time_index = "intentNotFound." + str(current_index) + "." + "time"
            group_index = "intentNotFound." + str(current_index) + "." + "group"
            time = pytz.utc.localize(datetime.datetime.utcnow())  # .strftime("%Y-%m-%d %H:%M")
            grv_id_row = "intentNotFound." + str(current_index)+ "." + "grv_id"
            update('status', self.session_id, {type_message_index: message})
            update('status', self.session_id, {time_index: time})
            update('status', self.session_id, {group_index: category})
            update('status', self.session_id, {grv_id_row: grv_id})

    def user_log(self, user_msg, ASRFlag, endSurveyFlag,guidedRestartFlag,group=None, grv_id=None):
        print("Inside user log")
        current_doc = get_by_id('status', self.session_id)
        try:
            print("Inside try")
            current_count = len(current_doc['conversations'])
            current_index = current_count
            type_message_index = "conversations." + str(current_index) + "." + "type"
            user_msg_index = "conversations." + str(current_index) + "." + "message"
            time_index =  "conversations." + str(current_index) + "." + "time"
            fuzzy_index = "conversations." + str(current_index) + "." + "FuzzySearch"
            asr_index = "conversations." + str(current_index) + "." + "ASR"
            end_survey_index = "conversations." + str(current_index) + "." + "endSurveyFlag"
            time = pytz.utc.localize(datetime.datetime.utcnow())#.strftime("%Y-%m-%d %H:%M")
            restartGuidedIndex = "conversations." + str(current_index) + "." + "restartGuidedIndex"
            groupIndex = "conversations." + str(current_index)+ "." + "groupIndex"
            grv_id_row = "conversations." + str(current_index)+ "." + "grv_id"
            error = False
            if user_msg != '':
                update('status', self.session_id, {user_msg_index: user_msg,
                                                   type_message_index: 'user message',
                                                   time_index: time,
                                                   fuzzy_index: error,
                                                   asr_index: ASRFlag,
                                                   end_survey_index: endSurveyFlag,
                                                   restartGuidedIndex: guidedRestartFlag,
                                                   groupIndex: group,
                                                   grv_id_row: grv_id
                                                   })

            if str(endSurveyFlag) == 'True':
                print(type(user_msg), user_msg)
                if str(user_msg) in '12345':
                    update('status', self.session_id, {'Rating': user_msg})
                elif user_msg.lower().strip() != gettext('yes') and user_msg.lower().strip() != gettext('no'):
                    if user_msg.lower().strip() not in ['main menu', 'Main Menu']:
                        update('status', self.session_id, {'Comments': user_msg})
                    else:
                        update('status', self.session_id, {'Comments': "None"})
        except Exception as e:
            print("Inside except")
            logger.exception('Exception: '+ str(e))
            current_index = 0
            type_message_index = "conversations." + str(current_index) + "." + "type"
            user_msg_index = "conversations." + str(current_index) + "." + "message"
            time_index = "conversations." + str(current_index) + "." + "time"
            fuzzy_index = "conversations." + str(current_index) + "." + "FuzzySearch"
            asr_index = "conversations." + str(current_index) + "." + "ASR"
            end_survey_index = "conversations." + str(current_index) + "." + "endSurveyFlag"
            time = pytz.utc.localize(datetime.datetime.utcnow())  # .strftime("%Y-%m-%d %H:%M")
            restartGuidedIndex = "conversations." + str(current_index) + "." + "restartGuidedIndex"
            groupIndex = "conversations." + str(current_index) + "." + "groupIndex"
            grv_id_row = "conversations." + str(current_index)+ "." + "grv_id"
            error = False
            if user_msg != '':
                update('status', self.session_id, {user_msg_index: user_msg,
                                                   type_message_index: 'user message',
                                                   time_index: time,
                                                   fuzzy_index: error,
                                                   asr_index: ASRFlag,
                                                   end_survey_index: endSurveyFlag,
                                                   restartGuidedIndex: guidedRestartFlag,
                                                   'Rating': "None",
                                                   'Comments': "None",
                                                   groupIndex: group,
                                                   grv_id_row: grv_id
                                                   })


    def bot_log(self, bot_msg, ASRFlag, endSurveyFlag,guidedRestartFlag,group=None, grv_id=None):
        current_doc = get_by_id('status', self.session_id)
        # print("Current doc:" , current_doc)
        logger.info(current_doc)
        try:
            current_count = len(current_doc['conversations'])
            print("Current count of conversations: ", current_count)
            current_index = current_count
            bot_msg_index = "conversations." + str(current_index) + "." + "message"
            type_message_index = "conversations." + str(current_index) + "." + "type"
            time_index = "conversations." + str(current_index) + "." + "time"
            time = pytz.utc.localize(datetime.datetime.utcnow())  # .strftime("%Y-%m-%d %H:%M")
            fuzzy_index = "conversations." + str(current_index) + "." + "FuzzySearch"
            asr_index = "conversations." + str(current_index) + "." + "ASR"
            end_survey_index = "conversations." + str(current_index) + "." + "endSurveyFlag"
            restartGuidedIndex = "conversations." + str(current_index) + "." + "restartGuidedIndex"
            groupIndex = "conversations." + str(current_index) + "."+ "groupIndex"
            grv_id_row = "conversations." + str(current_index)+ "." + "grv_id"

            if gettext("We couldn\'t find that. Do you mean: ") in bot_msg or gettext("Sorry we don\'t have an answer for that. Could you try again?") in bot_msg:
                error=True
            else:
                error=False

            update('status', self.session_id, {str(bot_msg_index): bot_msg,
                                               str(type_message_index): 'bot message',
                                               time_index: time,
                                               fuzzy_index: error,
                                               asr_index: ASRFlag,
                                               end_survey_index: endSurveyFlag,
                                               restartGuidedIndex: guidedRestartFlag,
                                               groupIndex: group,
                                               grv_id_row: grv_id
                                               })

        except Exception as e:
            logger.exception('Exception: '+str(e))
            current_index = 0
            bot_msg_index = "conversations." + str(current_index) + "." + "message"
            type_message_index = "conversations." + str(current_index) + "." + "type"
            time_index =  "conversations." + str(current_index) + "." + "time"
            fuzzy_index = "conversations." + str(current_index) + "." + "FuzzySearch"
            asr_index = "conversations." + str(current_index) + "." + "ASR"
            end_survey_index = "conversations." + str(current_index) + "." + "endSurveyFlag"
            restartGuidedIndex = "conversations." + str(current_index) + "." + "restartGuidedIndex"
            groupIndex = "conversations." + str(current_index) + "."+ "groupIndex"
            time = pytz.utc.localize(datetime.datetime.utcnow())#.strftime("%Y-%m-%d %H:%M")
            grv_id_row = "conversations." + str(current_index)+ "." + "grv_id"

            if gettext("We couldn\'t find that. Do you mean: ") in bot_msg or gettext("Sorry we don\'t have an answer for that. Could you try again?") in bot_msg:
                error=True
            else:
                error=False
            update('status', self.session_id, {str(bot_msg_index): bot_msg,
                                               str(type_message_index): 'bot message',
                                               time_index: time,
                                               fuzzy_index: error,
                                               asr_index: ASRFlag,
                                               end_survey_index: endSurveyFlag,
                                               restartGuidedIndex: guidedRestartFlag,
                                               groupIndex: group,
                                               grv_id_row: grv_id
                                               })

    # Method to update conversation of last index
    def update_last_conversation(self, message_to_be_updated):
        current_doc = get_by_id('status', self.session_id)
        try:
            current_count = len(current_doc['conversations'])
            print("Current count", current_count)
            last_index = current_count - 1
            # to_be_updated = current_doc['conversations'][last_index]
            bot_msg_index = "conversations." + str(last_index) + "." + "message"
            print("Bot_msg_index :", bot_msg_index)
            update('status', self.session_id, {str(bot_msg_index): message_to_be_updated})

        except Exception as e:
            logger.exception(e)
