import requests
import json
from math import sin, cos, sqrt, atan2, radians
import configparser
from utility.logger import logger

def latlongcalc(lat1, lon1, lat2, lon2):
    try:
        R = 6373.0

        lat1 = radians(lat1)
        lon1 = radians(lon1)
        lat2 = radians(lat2)
        lon2 = radians(lon2)

        dlon = lon2 - lon1
        dlat = lat2 - lat1

        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))

        distance = R * c * 0.621371
        return distance
    except Exception as e:
        logger.error("GCP Places error: ", str(e))

def retrieve_location(zip, type, radius_miles):
    try:
        pharmacy_list = get_pharmacies(zip, type, radius_miles)
        status, sorted_pharmacy_list = process_locations(pharmacy_list, radius_miles, zip, type)
        if status == 0:
            json_response = {
                "status": "Failure",
                "message": "No pharmacies in the area"
            }

        else:
            json_response = {
                "status": "Success",
                "message": sorted_pharmacy_list
            }
        return json_response

    except Exception as e:
        logger.error("GCP Places error: ", str(e))
        json_response = {
            "status": "Failure",
            "message": str(e)
        }
        return json_response

def get_pharmacies(zip, type, radius_miles):
    try:
        app_config = configparser.ConfigParser()
        app_config.read_file(open(r'config/app_settings.ini'))

        auth_key = 'AIzaSyAFcY1NATJ7oX6mxzw1DFCV09kYYE6fNkw'

        """
        Convert Zip to Lat/Long
        """
        url_zip = "https://maps.googleapis.com/maps/api/geocode/json?address=" + str(zip) + "&key=" + auth_key
        response_zip = requests.get(url_zip)
        response_json_zip = json.loads(response_zip.text)

        lat = response_json_zip['results'][0]['geometry']['location']['lat']
        long = response_json_zip['results'][0]['geometry']['location']['lng']

        """
        Retrieve pharmacies knowing:
        lat/long
        radius
        type = pharmacy
        """
        miles_to_meters = 1609.34
        # radius_miles = 5
        radius_meters = str(radius_miles * miles_to_meters)
        latlong = str(lat) + "," + str(long)
        url_places = "https://maps.googleapis.com/maps/api/place/search/json?location=" + latlong + "&radius=" + radius_meters + "&name=[name]&type=" + type + "&sensor=false&key=" + auth_key
        response_places = requests.get(url_places)
        response_json_places = json.loads(response_places.text)  # makes json of html_attributes, next_page_token, results, status
        # print(response_json_places)
        pharmacy_list = []

        """
        Calculate distance using distance matrix 
        """
        for item in response_json_places['results']:
            lat2 = item['geometry']['location']['lat']
            lon2 = item['geometry']['location']['lng']
            distance_matrix = latlongcalc(lat, long, lat2, lon2)

            destination = str(lat2) + ',' + str(lon2)

            addr = item['vicinity'].replace(' ', '+')
            store_name = str(item['name'].replace(' ', '+'))
            map_url = 'https://www.google.com/maps?source=uds&daddr=' + addr + '+(' + store_name + ')+%40' + destination + '&iwstate1=dir:to'
            pharmacy_list.append([item['name'], item['vicinity'], distance_matrix, str(distance_matrix)[0:3], map_url])
        return pharmacy_list
    except Exception as e:
        logger.error("GCP Places error: ", str(e))

def process_locations(pharmacy_list, radius, zip, type):
    try:
        app_config = configparser.ConfigParser()
        app_config.read_file(open(r'config/app_settings.ini'))


        if len(pharmacy_list) == 0:
            pass
            radius_temp = radius
            while len(pharmacy_list) == 0 or radius_temp < 25:
                radius_temp = radius_temp * 2
                print(radius_temp)
                pharmacy_list = get_pharmacies(zip, type, radius_temp)
        sorted_pharmacy_list = sorted(pharmacy_list, key=lambda x: x[3])

        sorted_pharmacy_list = [[y[0], y[1], str(y[3]) + " mi", y[4]] for y in sorted_pharmacy_list]

        num_locations = app_config.get('GENERAL', 'number_locations')

        sorted_pharmacy_list = sorted_pharmacy_list[0:int(num_locations)]

        if len(sorted_pharmacy_list) == 0 :
            return 0, ""
        else:
            return 1, sorted_pharmacy_list
    except Exception as e:
        logger.error("GCP Places error: ", str(e))

# ans = retrieve_location(75093, "pharmacy", 5)
# print(ans)
