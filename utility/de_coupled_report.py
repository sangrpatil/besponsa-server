from pymongo import MongoClient
import os
import pytz
from dateutil import tz
from bson.objectid import ObjectId
import re
from utility.mongo_dao import *
from utility.logger import logger
# from utility.generate_reports_json import utc_to_chinese
from utility.generate_reports_excel import word_check_list, key_list_gen

import datetime
from datetime import datetime
from datetime import timedelta

host = os.environ.get('MONGO_HOST') or 'localhost'
port = os.environ.get('MONGO_PORT') or '27017'
username = os.environ.get('MONGO_USERNAME') or 'admin'
password = os.environ.get('MONGO_PASSWORD') or 'admin'
database = os.environ.get('MONGO_DB') or 'admin_besponsa'

client = MongoClient('mongodb://'+username+':'+password+'@'+host+':'+str(port)+'/admin')
to_zone = tz.gettz('UTC')

def get_dashboard(bot_id=None,group=None,bot_name=''):
    print("Entered get_dashboard")
    try:
        ratingKeyList=[0,1,2,3,4,5]
        rating = client[database]["status"].aggregate([

            {
            "$group":{"_id": "$Rating",
                        "count": { "$sum": 1 }
                        }
            }

        ])
        ratingValueList= [0]*6
        print(ratingValueList)
        for i in rating:
            j = i.get('_id')
            print(i)
            if i.get('_id') == 'None' or not i.get('_id'):
                j = 0
            ratingValueList[int(j)] += i['count']
            print(i.get('count'))
        print(ratingValueList)


        ##################### Number of Questions By Day ####################
        print("Getting Dash Data ")
        data=get_DashData(bot_id,group)
        dateKeyList=[]
        dateValuesList=[]
        print("Print Data", data['dateDict'])
        for key, value in sorted(data["dateDict"].items()):
            dateKeyList.append(key)
            value = len(set(value))
            dateValuesList.append(value)
        ######################## Peak Engagement Hours #########################
        timeKeyList=[]
        timeValueList=[]
        for key, value in sorted(data["timeDict"].items()):
            if key == 12:
                key = '12 pm'
            elif key == 24 or key == 0:
                key = '12 am'
            elif (key - 12) > 1:
                key = key - 12
                key = str(key) + ' pm'
            else:
                key = str(key) + ' am'

            timeKeyList.append(key)
            value = len(set(value))
            timeValueList.append(value)
        ################################ User Satisfaction Distribution #################
        ratingCountKeyList=[]
        ratingCountValueList=[]
        for key, value in sorted(data["ratingHistogramDict"].items()):
            ratingCountKeyList.append(key)
            ratingCountValueList.append(value)

        ################################# Questions Answered by Bot per Group ###########
        botAnswerKeyList=[]
        botAnswerValueList=[]
        for key, value in data["botAnsweredDict"].items():
            botAnswerKeyList.append(key)
            botAnswerValueList.append(value)

        ########################## Question Response Summary #######################
        questionAnswerKeyList=[]
        questionAnswerValueList=[]
        for key, value in data["questionResponseDict"].items():
            questionAnswerKeyList.append(key)
            questionAnswerValueList.append(value)
        ########################## LATENCY GRAPH ##############################
        latencyKeyList=[]
        latencyValueList=[]
        for key, value in sorted(data["latencyDict"].items()):
            if key == 12:
                key = '12 pm'
            elif key == 24 or key == 0:
                key = '12 am'
            elif (key - 12) > 1:
                key = key - 12
                key = str(key) + ' pm'
            else:
                key = str(key) + ' am'
            latencyKeyList.append(key)
            if value == []:
                finalLatencyAvg = 0
            else:
                if value['numConvos'] != 0:
                    finalLatencyAvg = value['latencySum'] / value['numConvos']
                else:
                    finalLatencyAvg = 0
            latencyValueList.append(finalLatencyAvg)
        ##########################################################

        final_json = {
            "bot_name": bot_name,
            "graph_data": [
                {
                    "graph_name": "Number of Questions By Day",
                    "x_values": dateKeyList,
                    "y_values": dateValuesList,
                    "x_axis_title": "Days",
                    "y_axis_title": "Number of Questions"
                },
                {
                    "graph_name": "Peak Engagement Hours",
                    "x_values":timeKeyList,
                    "y_values": timeValueList,
                    "x_axis_title": "Time of Day (Asia/Shanghai)",
                    "y_axis_title": "Number of Sessions"
                },
                {
                    "graph_name": "User Satisfaction",
                    "x_values": ratingKeyList,
                    "y_values": ratingValueList,
                    "x_axis_title": "Days",
                    "y_axis_title": "Average Satisfaction Rating"
                },
                {
                    "graph_name": "Bot Response Latency",
                    "x_values": latencyKeyList,
                    "y_values": latencyValueList,
                    "x_axis_title": "Time of Day (Asia/Shanghai)",
                    "y_axis_title": "Average Response Latency (sec)"
                },
                {
                    "graph_name": "User Satisfaction Distribution",
                    "x_values": ratingCountKeyList,
                    "y_values": ratingCountValueList,
                    "x_axis_title": "Rating Score",
                    "y_axis_title": "Number of Ratings"
                },
                {
                    "graph_name": "Questions Answered by Bot per Group",
                    "x_values": botAnswerKeyList,
                    "y_values": botAnswerValueList,
                    "x_axis_title": "Groups",
                    "y_axis_title": "Number of Questions Answered"
                },
                {
                    "graph_name": "Question Response Summary",
                    # x values  - questionAnswerKeyList
                    # y values  -questionAnswerValueList
                    "bar1": {
                        'x': questionAnswerKeyList,
                        'y': [questionAnswerValueList[0]['Bot'], questionAnswerValueList[1]['Bot']],
                        'name': 'Bot',
                        'type': 'bar'
                    },
                    "bar2": {
                        'x': questionAnswerKeyList,
                        'y': [questionAnswerValueList[0]['Admin'], questionAnswerValueList[1]['Admin']],
                        'name': 'Admin',
                        'type': 'bar'
                    },
                    "x_axis_title": "Bot/Admin",
                    "y_axis_title": "Number of Questions"
                }
            ]
        }
        return final_json
    except Exception as e:
        print(e)
        logger.exception(e)
        return "Failure"

def get_intent_found(start, end, bot_id,group=None,offset=0,next=50):
    try:
        start_date, end_date = start_end(start, end)

        res = client[database]["status"].aggregate([
            {
                "$match": {"$and": [

                    {"bot_id": {"$eq": bot_id}},
                    {"intentFound": {
                        "$exists": "true"
                    }}]
                }

            },
            {
                "$project": {
                    "wordsAsKeyValuePairs": {
                        "$objectToArray": "$intentFound"
                    },
                    "id": "$_id",
                    "user": {"$ifNull": ['$user_id', 'None']}
                }
            },
            {
                "$unwind": "$wordsAsKeyValuePairs"
            },
            {"$sort": {'wordsAsKeyValuePairs.v.time': -1}},
            {
                "$match": {"$and": [

                    {"wordsAsKeyValuePairs.v.time": {"$gte": start_date}},
                    {"wordsAsKeyValuePairs.v.time": {"$lte": end_date}}]
                }

            },

            {
                "$project": {"row": {
                    "QUESTION ID": "$id",
                    "USER QUESTION": "$wordsAsKeyValuePairs.v.intentFound",
                    "KEYWORDS": "$wordsAsKeyValuePairs.v.keywords",
                    "DATE": {"$concat": [
                         {"$substr": [{"$year": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 4]}, "-",
                         {"$substr": [{"$month": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}, "-",
                         {"$substr": [{"$dayOfMonth": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}
                                      ]},
                    "TIME (UTC)": {"$concat": [
                        {"$substr": [{"$hour": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}, ":",
                        {"$substr": [{"$minute": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}
                    ]},
                    # "SLA REMAINING TIME": {"$concat": [
                    #     {"$substr": [{"$hour": "$wordsAsKeyValuePairs.v.time"}, 0, 2]}, ":",
                    #     {"$substr": [{"$minute": "$wordsAsKeyValuePairs.v.time"}, 0, 2]}, ":",
                    #     {"$substr": [{"$second": "$wordsAsKeyValuePairs.v.time"}, 0, 4]}
                    # ]},
                    "SLA REMAINING TIME": {
                        "$cond": {"if": {"$eq": ["$wordsAsKeyValuePairs.v.adminResponse", "No"]}, "then": {"$concat": [
                            {"$substr": [{"$hour": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}, " hours"
                        ]}, "else": "N/A"}
                    },
                    "USER ID": "$user",
                    "grv_id": {"$ifNull": ["$wordsAsKeyValuePairs.v.grv_id", 'None']},
                    "ADMIN RESPONSE": {"$ifNull": ["$wordsAsKeyValuePairs.v.adminResponse", 'No']},

                    "SESSION ID": "$wordsAsKeyValuePairs.v.sesssionID",

                    "QUESTION CATEGORY": {
                        "$cond": {"if": {"$eq": ["$wordsAsKeyValuePairs.v.adminResponse", "No"]},
                                  "then": "$wordsAsKeyValuePairs.v.groupIndex", "else": ["None"]}

                    },
                    "UNIQUE ID": {"$ifNull": ["$wordsAsKeyValuePairs.v.uniqueID",
                                              {"$concat": ["$wordsAsKeyValuePairs.v.sesssionID", "ID"]}]},

                }
                }},
            {"$group": {"_id": None, "myCount": {"$sum": 1}, "convo": {"$push": "$row"}}},
            {"$project": {
                "_id": 0,
                "total": "$myCount",
                "intent": {"$slice": ["$convo", offset, next]}}}

        ])

        result=[i for i in res]
        count=0
        id_dic={}
        for j in result[0]["intent"]:
            if j['QUESTION ID'] in id_dic:
                j['QUESTION ID']=id_dic[j['QUESTION ID']]
            else:
                count += 1
                id_dic[j['QUESTION ID']]=count
                j['QUESTION ID'] = id_dic[j['QUESTION ID']]




        return result[0]["intent"],result[0]["total"]
    except Exception as e:
        print(e)
        return [],0
        #return "Failure",0

def get_intent_NotFound(start, end, bot_id,group=None,offset=0,next=50):
    try:
        start_date, end_date = start_end(start, end)
        res=client[database]["status"].aggregate([
            {
                "$match": {"$and": [

                    {"bot_id": {"$eq": bot_id}},
                    {"intentNotFound": {
                        "$exists": "true"
                    }}]
                }

            },
            {
                "$project": {
                    "wordsAsKeyValuePairs": {
                        "$objectToArray": "$intentNotFound"
                    },
                    "id": "$_id",
                    "user": {"$ifNull": ['$user_id', 'None']}
                }
            },
            {
                "$unwind": "$wordsAsKeyValuePairs"
            },
            {"$sort": {'wordsAsKeyValuePairs.v.time': -1}},
            {
                "$match": {"$and": [

                    {"wordsAsKeyValuePairs.v.time": {"$gte": start_date}},
                    {"wordsAsKeyValuePairs.v.time": {"$lte": end_date}}]
                }

            },
            {
                "$project": {"row": {
                "QUESTION ID": "$id",
                "USER QUESTION": "$wordsAsKeyValuePairs.v.intentNotFound",
                "KEYWORDS": "$wordsAsKeyValuePairs.v.keywords",
                "DATE": {"$concat": [
                         {"$substr": [{"$year": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 4]}, "-",
                         {"$substr": [{"$month": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}, "-",
                         {"$substr": [{"$dayOfMonth": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}
                                      ]},
                "TIME (UTC)": {"$concat": [
                              {"$substr": [{"$hour": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}, ":",
                              {"$substr": [{"$minute": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}
                                  ]},
                "SLA REMAINING TIME":{
                        "$cond": { "if": {"$eq": ["$wordsAsKeyValuePairs.v.adminResponse", "No"]}, "then":{"$concat": [
                           {"$substr": [{"$hour": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}," hours"
                                        ]}, "else":"N/A"}
                },
        "grv_id": {"$ifNull": ["$wordsAsKeyValuePairs.v.grv_id",'None']},
        "USER ID": "$user",
        "ADMIN RESPONSE": {"$ifNull": ["$wordsAsKeyValuePairs.v.adminResponse",'No']},
        "SESSION ID": "$wordsAsKeyValuePairs.v.sesssionID",
        "QUESTION CATEGORY":{
            "$cond": { "if": {"$eq": ["$wordsAsKeyValuePairs.v.adminResponse", "No"]},
                       "then": "$wordsAsKeyValuePairs.v.groupIndex", "else":["None"]}

        },
        "UNIQUE ID": {"$ifNull": ["$wordsAsKeyValuePairs.v.uniqueID",{ "$concat": [ "$wordsAsKeyValuePairs.v.sesssionID", "ID"] } ]},


        }
        }},
        {"$group": {"_id": None,"myCount": { "$sum": 1 }, "convo": {"$push": "$row"}}},
        {"$project": {
            "_id": 0,
            "total":"$myCount",
            "intent": {"$slice": ["$convo", offset, next]}}}


        ])

        result=[i for i in res]
        count = 0
        id_dic = {}
        for j in result[0]["intent"]:
            if j['QUESTION ID'] in id_dic:
                j['QUESTION ID'] = id_dic[j['QUESTION ID']]
            else:
                count += 1
                id_dic[j['QUESTION ID']] = count
                j['QUESTION ID'] = id_dic[j['QUESTION ID']]
        return result[0]["intent"],result[0]["total"]
    except Exception as e:
        print(e)
        return [], 0
        #return "Failure",0

def get_ConvoLog(start,end,bot_id,group=None,offset=0,next=50):
    start_date, end_date = start_end(start, end)
    try:
        res=client[database]["status"].aggregate([
            {
                "$match": {"$and": [

            {"bot_id": {"$eq": bot_id}},
        {"conversations": {
            "$exists": "true"
        }}]
        }

        },
            {
                "$project": {
                    "wordsAsKeyValuePairs": {
                        "$objectToArray": "$conversations"
                    },
                    "rating":{"$ifNull": ["$Rating", 'None'] },
                    "comment": {"$ifNull": ['$Comments','None']},
                    "user": {"$ifNull": ['$user_id', 'None']},
                    "id": "$_id"
                }
            },
            {
                "$unwind": "$wordsAsKeyValuePairs"
            },
        {"$sort": {'wordsAsKeyValuePairs.v.time': -1}},
            {
                "$match": {"$and": [

                    {"wordsAsKeyValuePairs.v.time": {"$gte": start_date}},
                    {"wordsAsKeyValuePairs.v.time": {"$lte": end_date}}]
                }

            },


        # {"$redact": {
            #     "$cond": [
            #         {"$in": [
            #             {"$let": {
            #                 "vars": {
            #                     "item": {"$arrayElemAt": ["$wordsAsKeyValuePairs.v.groupIndex", -1]}
            #                 },
            #                 "in": "$$item"
            #             }},
            #             group
            #         ]},
            #         "$$KEEP",
            #         "$$PRUNE"
            #     ]
            # }},
            #{"$group": {"_id": None, "convo": {"$addToSet": "$wordsAsKeyValuePairs.v"}}}
            {
                "$project": {"row": {
                    "ID": "$id",
                    "FROM": {
                        "$cond": { "if": {"$eq": ["$wordsAsKeyValuePairs.v.type",
                                                "bot message"]}, "then": "ChatBot", "else":"User"}}
        ,
        "MESSAGE": "$wordsAsKeyValuePairs.v.message",
        "ASR/TTS USAGE": {
            "$cond": { "if": {"$eq": ["$wordsAsKeyValuePairs.v.ASR", False]}, "then": "No", "else":"Yes"}},
        "DATE": {"$concat": [
                         {"$substr": [{"$year": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 4]}, "-",
                         {"$substr": [{"$month": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}, "-",
                         {"$substr": [{"$dayOfMonth": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}
                                      ]},
        "TIME (UTC)": {"$concat": [
            {"$substr": [{"$hour": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}, ":",
            {"$substr": [{"$minute": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}, ":",
            {"$substr": [{"$second": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 4]}
        ]},
        "USER ID": {"$ifNull": ["$user",'None']},
        "RATING": "$rating",
        "COMMENTS": "$comment",
        "QUESTION CATEGORY": {"$ifNull": ["$wordsAsKeyValuePairs.v.groupIndex",'None']},
        "SURVEY RESPONSES": {
            "$cond": { "if": {"$eq": ["$wordsAsKeyValuePairs.v.endSurveyFlag", False]}, "then": "No", "else":"Yes"}},
        "grv_id": {"$ifNull": ["$wordsAsKeyValuePairs.v.grv_id",'None']},
        "AE": {"$ifNull": ["$wordsAsKeyValuePairs.v.ae",'None']},
        }
        }},
            {"$group": {"_id": None, "myCount": {"$sum": 1}, "convo": {"$push": "$row"}}},
            {"$project": {
                "_id": 0,
                "total": "$myCount",
                "intent": {"$slice": ["$convo", offset, next]}}}

        ])
        result=[i for i in res]
        count = 0
        id_dic = {}
        for j in result[0]["intent"]:
            if j['ID'] in id_dic:
                j['ID'] = id_dic[j['ID']]
            else:
                count += 1
                id_dic[j['ID']] = count
                j['ID'] = id_dic[j['ID']]


        # adverse_key_list = key_list_gen()
        #
        # for x in result[0]["intent"]:
        #     Adverse_Events = word_check_list(x['MESSAGE'], adverse_key_list)
        #     x.update({"Adverse Event": Adverse_Events})

        return result[0]["intent"],result[0]["total"]
    except Exception as e:
        print(e)
        return [], 0
        #return "Failure",0

def get_DashData(bot_id,group=None):
    try:
        resintentnot= client[database]["status"].aggregate([
            {
                "$match": {"$and": [

                    {"bot_id": {"$eq": bot_id}},
                    {"intentNotFound": {
                        "$exists": "true"
                    }}]
                }

            },
            {
                "$project": {
                    "wordsAsKeyValuePairs": {
                        "$objectToArray": "$intentNotFound"
                    },
                    "id": "$_id",
                    "user": {"$ifNull": ['$user_id', 'None']}
                }
            },
            {
                "$unwind": "$wordsAsKeyValuePairs"
            },
        {"$sort": {'wordsAsKeyValuePairs.v.time': -1}},




        {
                "$project": {"row": {
                    "QUESTION ID": "$id",
                    "USER QUESTION": "$wordsAsKeyValuePairs.v.intentNotFound",
                    "KEYWORDS": "$wordsAsKeyValuePairs.v.keywords",
                    "DATE": {"$concat": [
                         {"$substr": [{"$year": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 4]}, "-",
                         {"$substr": [{"$month": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}, "-",
                         {"$substr": [{"$dayOfMonth": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}
                                      ]},
                    "TIME (UTC)": {"$concat": [
                        {"$substr": [{"$hour": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}, ":",
                        {"$substr": [{"$minute": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}
                    ]},

                    "SLA REMAINING TIME": {
                        "$cond": {"if": {"$eq": ["$wordsAsKeyValuePairs.v.adminResponse", "No"]}, "then": {"$concat": [
                            {"$substr": [{"$hour": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}, " hours"
                        ]}, "else": "N/A"}
                    },
                    "USER ID": "$user",
                    "ADMIN RESPONSE": {"$ifNull": ["$wordsAsKeyValuePairs.v.adminResponse", 'No']},

                    "SESSION ID": "$wordsAsKeyValuePairs.v.sesssionID",

                    "QUESTION CATEGORY": {
                        "$cond": {"if": {"$eq": ["$wordsAsKeyValuePairs.v.adminResponse", "No"]},
                                  "then": "$wordsAsKeyValuePairs.v.group", "else": ["None"]}

                    },
                    "Group":"$wordsAsKeyValuePairs.v.group",
                    "UNIQUE ID": {"$ifNull": ["$wordsAsKeyValuePairs.v.uniqueID",
                                              {"$concat": ["$wordsAsKeyValuePairs.v.sesssionID", "ID"]}]},

                }
                }},
            {"$group": {"_id": None, "myCount": {"$sum": 1}, "convo": {"$push": "$row"}}},

        ])
        resintent = client[database]["status"].aggregate([
            {
                "$match": {"$and": [

                    {"bot_id": {"$eq": bot_id}},
                    {"intentFound": {
                        "$exists": "true"
                    }}]
                }

            },
            {
                "$project": {
                    "wordsAsKeyValuePairs": {
                        "$objectToArray": "$intentFound"
                    },
                    "id": "$_id",
                    "user": {"$ifNull": ['$user_id', 'None']}
                }
            },
            {
                "$unwind": "$wordsAsKeyValuePairs"
            },
            {"$sort": {'wordsAsKeyValuePairs.v.time': -1}},
            {
                "$project": {"row": {
                    "QUESTION ID": "$id",
                    "USER QUESTION": "$wordsAsKeyValuePairs.v.intentFound",
                    "KEYWORDS": "$wordsAsKeyValuePairs.v.keywords",
                    "DATE": {"$concat": [
                         {"$substr": [{"$year": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 4]}, "-",
                         {"$substr": [{"$month": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}, "-",
                         {"$substr": [{"$dayOfMonth": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}
                                      ]},
                    "TIME (UTC)": {"$concat": [
                        {"$substr": [{"$hour": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}, ":",
                        {"$substr": [{"$minute": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}
                    ]},
                    # "SLA REMAINING TIME": {"$concat": [
                    #     {"$substr": [{"$hour": "$wordsAsKeyValuePairs.v.time"}, 0, 2]}, ":",
                    #     {"$substr": [{"$minute": "$wordsAsKeyValuePairs.v.time"}, 0, 2]}, ":",
                    #     {"$substr": [{"$second": "$wordsAsKeyValuePairs.v.time"}, 0, 4]}
                    # ]},
                    "SLA REMAINING TIME": {
                        "$cond": {"if": {"$eq": ["$wordsAsKeyValuePairs.v.adminResponse", "No"]}, "then": {"$concat": [
                            {"$substr": [{"$hour": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}, " hours"
                        ]}, "else": "N/A"}
                    },
                    "USER ID": "$user",
                    "ADMIN RESPONSE": {"$ifNull": ["$wordsAsKeyValuePairs.v.adminResponse", 'No']},

                    "SESSION ID": "$wordsAsKeyValuePairs.v.sesssionID",

                    "QUESTION CATEGORY": {
                        "$cond": {"if": {"$eq": ["$wordsAsKeyValuePairs.v.adminResponse", "No"]},
                                  "then": "$wordsAsKeyValuePairs.v.group", "else": ["None"]}

                    },
                    "Group":"$wordsAsKeyValuePairs.v.group",
                    "UNIQUE ID": {"$ifNull": ["$wordsAsKeyValuePairs.v.uniqueID",
                                              {"$concat": ["$wordsAsKeyValuePairs.v.sesssionID", "ID"]}]},

                }
                }},
            {"$group": {"_id": None, "myCount": {"$sum": 1}, "convo": {"$push": "$row"}}},

        ])
        resconvo = client[database]["status"].aggregate([
            {
                "$match": {"$and": [

                    {"bot_id": {"$eq": bot_id}},
                    {"conversations": {
                        "$exists": "true"
                    }}]
                }

            },
            {
                "$project": {
                    "wordsAsKeyValuePairs": {
                        "$objectToArray": "$conversations"
                    },
                    "rating": {"$ifNull": ["$Rating", 'None']},
                    "comment": {"$ifNull": ['$Comments', 'None']},
                    "user": {"$ifNull": ['$user_id', 'None']},
                    "id": "$_id"

                }
            },
            {
                "$unwind": "$wordsAsKeyValuePairs"
            },
            {"$sort": {'wordsAsKeyValuePairs.v.time': -1}},
            # {"$group": {"_id": None, "convo": {"$addToSet": "$wordsAsKeyValuePairs.v"}}}
            {
                "$project": {"row": {
                    "ID": "$id",
                    "time":"$wordsAsKeyValuePairs.v.time",
                    "FROM": {
                        "$cond": {"if": {"$eq": ["$wordsAsKeyValuePairs.v.type",
                                                 "bot message"]}, "then": "ChatBot", "else": "User"}}
                    ,
                    "MESSAGE": "$wordsAsKeyValuePairs.v.message",
                    "ASR/TTS USAGE": {
                        "$cond": {"if": {"$eq": ["$wordsAsKeyValuePairs.v.ASR", "False"]}, "then": "No",
                                  "else": "Yes"}},
                    "DATE": {"$concat": [
                         {"$substr": [{"$year": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 4]}, "-",
                         {"$substr": [{"$month": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}, "-",
                         {"$substr": [{"$dayOfMonth": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}
                                      ]},
                    "TIME (UTC)": {"$concat": [
                        {"$substr": [{"$hour": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}, ":",
                        {"$substr": [{"$minute": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 2]}, ":",
                        {"$substr": [{"$second": {"date":"$wordsAsKeyValuePairs.v.time","timezone":'Asia/Shanghai'}}, 0, 4]}
                    ]},
                    "USER ID": {"$ifNull": ["$user", 'None']},
                    "RATING": "$rating",
                    "COMMENTS": "$comment",
                    "SURVEY RESPONSES": {
                        "$cond": {"if": {"$eq": ["$wordsAsKeyValuePairs.v.endSurveyFlag", "False"]}, "then": "No",
                                  "else": "Yes"}},

                }
                }},
            {"$group": {"_id": None, "myCount": {"$sum": 1}, "convo": {"$push": "$row"}}},


        ])
        try:
            intnotfound = [i for i in resintentnot][0]["convo"]
        except Exception as e:
            print("In Exception: Intent not found")
            print(e)
            intnotfound=[]
        result=intnotfound#[i for i in resintentnot][0]["convo"]
        try:
            intfound=[i for i in resintent][0]["convo"]
        except Exception as e:
            print(e)
            print("in Exception Entener tfound")
            intfound=[]
        print("after exception in intenet found")
        result.extend(intfound)
        ratingHist = client[database]["status"].aggregate([
            {
                "$match":
                    {"bot_id": {"$eq": bot_id}}

            },
            {
                "$project": {

                    "rating": {"$ifNull": ["$Rating", 'None']},
                }
            }, ])
        result1 = [i for i in resconvo]
        count = 0
        id_dic = {}
        try:
            for j in result1[0]["convo"]:
                if j['ID'] in id_dic:
                    j['ID'] = id_dic[j['ID']]
                else:
                    count += 1
                    id_dic[j['ID']] = count
                    j['ID'] = id_dic[j['ID']]
            convo = [i for i in result1][0]["convo"]
        except Exception as e:
            convo=[]
        dateDict={}
        timeDict = {}
        latencyDict = {}
        ratingHistogramDict = {'No Rating': 0, '1': 0, '2': 0, '3': 0, '4': 0, '5': 0}
        for k in range(0, 25):
            timeDict[k] = []
            latencyDict[k] = []
        for z in result1[0]["convo"]:
            if z["DATE"] in dateDict:
                dateDict[z["DATE"]].append(z["ID"])
            else:
                dateDict[z["DATE"]] = [z["ID"]]
        #convo=[i for i in result1][0]["convo"]
        botJustSentMessage = True
        userReplyTime = datetime.utcnow()
        for j in convo:
            hour=int(j["TIME (UTC)"].split(":")[0])
            if hour in timeDict:
                timeDict[hour].append(j["ID"])
            else:
                timeDict[hour] = [j["ID"]]
            if hour in latencyDict:
                if latencyDict[hour] == []:
                    # only want to calculate "bot message timestamp" - "user message timestamp"
                    if j['FROM'] == 'User':
                        userReplyTime = j['time']

                    latencyDict[hour] = {'numConvos': 0, 'latencySum': 0}  # not time
                elif j['FROM'] == 'User':
                    userReplyTime = j['time']
                    print(userReplyTime)
                    # userReplyTime = time
                    botJustSentMessage = False
                elif j['FROM'] == 'ChatBot' and botJustSentMessage == False:
                    #datetime.strptime(j['TIME (UTC)'], '%Y-%m-%d %H:%M:%S.%f')
                        print("time ---->",j['time'])
                        latency =(userReplyTime - j['time'] ).total_seconds()
                        numConvosLat = 1 + latencyDict[hour]['numConvos']
                        latencySum = latencyDict[hour]['latencySum'] + latency
                        latencyDict[hour] = {'numConvos': numConvosLat, 'latencySum': latencySum}
                        botJustSentMessage = True

        for l in ratingHist:
            if l['rating'] != "None":
                ratingHistogramDict[str(l['rating'])]+= 1
            else:
                ratingHistogramDict['No Rating'] += 1

        questionResponseDict = {"Answered": {"Bot": 0, "Admin": 0}, "Unanswered": {"Bot": 0, "Admin": 0}}
        botAnsweredDict={}
        #intfound=[i for i in resintent][0]["convo"]
        print("intent found" ,intfound)
        print("Intent not found ", intnotfound)
        print("Conversation: ", convo)
        for value in intfound:
            if value['Group'] in group:
                if value['Group'] in botAnsweredDict:
                    botAnsweredDict[value['Group']] += 1
                else:
                    botAnsweredDict[value['Group']] = 1
                questionResponseDict['Answered']['Bot'] += 1
        #intnotfound = [i for i in resintentnot][0]["convo"]
        for value in intnotfound:
            questionResponseDict['Unanswered']['Bot'] += 1
            if value['ADMIN RESPONSE'] == 'No':
                questionResponseDict['Unanswered']['Admin'] += 1
            elif value['ADMIN RESPONSE'] == 'Yes':
                questionResponseDict['Answered']['Admin'] += 1

        return {"dateDict":dateDict,
                "timeDict":timeDict,
                "ratingHistogramDict":ratingHistogramDict,
                "questionResponseDict":questionResponseDict,
                "botAnsweredDict":botAnsweredDict,
                "latencyDict":latencyDict
                }
    except Exception as e:
        print(e)
        logger.exception(e)
        return "Failure"
#print(get_intent_found(['扫码签到介绍','企业号注册']))
# print(get_ConvoLog('5c6e8a3445a899832c03e7da',['扫码签到介绍','企业号注册']))
# print(get_dashboard())


def start_end(start,end):
    kor = pytz.timezone('Asia/Shanghai')
    if start == '':
        start = "1000-01-01"
        start_date = datetime.strptime(start, '%Y-%m-%d')
        print("start date: ", start_date)
        # start_date = start_date.astimezone(to_zone).strftime('%Y-%m-%d')
    else:
        print("Start received: ", start)
        start_date = datetime.strptime(start, '%Y-%m-%d')
        print("received korea time: ", start_date,  kor.localize(start_date))
        start_date = kor.localize(start_date).astimezone(to_zone)
        print("start date else: ", start_date)

    if end == '':
        end = str(datetime.utcnow()+timedelta(days=1)).split(' ')[0]
        print("end +1 date: ", end)
        #end_date = str(datetime.datetime.utcnow()+timedelta(days=1)).split(' ')[0]
        end_date = datetime.strptime(end, '%Y-%m-%d')
        print("end date:",end_date)
        # end_date = kor.localize(end_date).astimezone(to_zone).strftime('%Y-%m-%d')
    else:
        end_date = datetime.strptime(end, '%Y-%m-%d')
        end_date = (kor.localize(end_date).astimezone(to_zone) + timedelta(days=1))  # .strftime('%Y-%m-%d')
        print("end date:", end_date)

    return start_date,end_date
