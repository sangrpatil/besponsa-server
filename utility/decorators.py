from flask import Blueprint, request, jsonify, current_app, render_template, session, redirect, Markup, url_for, flash
from functools import wraps
from utility.mongo_dao import *

# Decorators
from utility.session_manager import get as get_context, set as set_context, modify as update_context




DOES_NOT_NEED_LOGIN = True

def is_logged_in():
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            if 'loggedIn' in session and session["loggedIn"]:
                return f(*args, **kwargs)
            else:
                return render_template('login.html')

        return wrapped

    return wrapper


def admin_check(*session):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            # session=get_context(session_id)
            if 'username' in session and session["username"] == 'admin':
                return f(*args, **kwargs)
            else:
                return jsonify(
                    status='Error',
                    msg="You are not allowed to access this page."
                )

        return wrapped

    return wrapper


def requires_roles_groups(*roles):
    """
    Decorator for checking loggin, allowed roles ,group and allowed bot ids
    :param roles: dictionary containing allowed roles , groups and bot ids
    :return:
    """

    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            print(roles)
            if 'loggedIn' in session and session["loggedIn"]:
                if 'username' in session:
                    user = get_one('user', 'username', session['username'])
                    group = [i['_id'] for i in user.get('group')]
                    print(group, "*****")
                    role_list = list_query('role', 'groups', group)
                    print(role_list, "role list")
                    role_list = [j['name'] for j in role_list[0].get('role')]
                    # for j in group:
                    #     for k in j:
                    #         role.append((k.get('name')).lower())
                    #
                    #
                    if 'role' in roles[0]:
                        allowed_roles = [x.lower() for x in roles[0].get('role')]
                        role_flag = False
                        for t in allowed_roles:
                            if t in role_list:
                                role_flag = True
                                break
                    permission_flag = False
                    if 'permission' in roles[0]:
                        allowed_permissions = [x.lower() for x in roles[0].get('permission')]
                        for t in allowed_permissions:
                            if t in user.get('permission'):
                                permission_flag = True
                                break
                        if not permission_flag:  # not role_flag or
                            return jsonify(
                                status='Error',
                                msg="You are not allowed to access this page."
                            )
                    else:
                        if 'bot_id' in roles[0]:
                            print("here")
                            bot_id = kwargs.get('bot_id')
                            if bot_id in roles[0].get('bot_id'):
                                return f(*args, **kwargs)
                            else:
                                return jsonify(
                                    status='Error',
                                    msg="You are not allowed to access this page."
                                )

                        return f(*args, **kwargs)
            else:
                return render_template('login.html')

        return wrapped

    return wrapper


# def requires_admin(*something):
#     """
#     Decorator for checking loggin, allowed roles ,group and allowed bot ids
#     :param roles: dictionary containing allowed roles , groups and bot ids
#     :return:
#     """
#     print(something)
#     def wrapper(f):
#         @wraps(f)
#         def wrapped(*args, **kwargs):
#             #print(roles)
#             print("In decorator" ,session)
#             if 'loggedIn' in session and session["loggedIn"]:
#                 if 'username' in session and 'group' in session:
#                     user=get_one('user','username',session['username'])

#                     #check if user contains that group
#                     """
#                     db.getCollection('groups').find({"user_list.name": {"$eq":'admin'},
#                         "name": {"$eq":"Hong Kong"}})

#                     """

#                     temp = {
#                         "user_list.name" : session['username'],
#                         "name": session['group']
#                     }
#                     cursor = get_all_with_multiple_conditions("group",temp)
#                     if not cursor:
#                         return jsonify(
#                             status='Error',
#                             msg="You do not have access to this group "
#                         )


#                     # if currsor is valid document, the user exists in gthe grroup
#                     # hence is part of that group

#                     # now user permission needs to be check , if he is admin or general

#                     #get the permission of user

#                     users_group_list = user.get('group')

#                     for i in users_group_list:
#                         if i['name'] == session.get('group'):
#                             permission = i['permission']
#                             break

#                     if permission in something[0]['role']:
#                         return f(*args, **kwargs)
#                     else:
#                         return jsonify(
#                             status='Error',
#                             msg="You do not have permission to perform this operation "
#                         )

#             else:
#                 return render_template('login.html')
#         return wrapped
#     return wrapper


def requires_admin(*something):
    def wrapper(f):
        # print("Inside def wrapper")
        @wraps(f)

        def wrapped(*args, **kwargs):

            if DOES_NOT_NEED_LOGIN:
                return f(*args, **kwargs)

            print("inside wrapped",request.json,request.form)

            message = request.get_json(force=True, silent=True)
            print("request in decorarot", request.json)
            if not message:
                message = request.form
            print("Resqeust data in dec", message)
            username = None

            sessionObj = message.get('sessionObj',None)

            if sessionObj:
                username = message.get('sessionObj').get('user_name',None)
            else:
                username = message.get('user_name', None)



            print("Request data in wrapped: ", message)
            if username:



                user = get_one("user", "username", username)

                if user['type'] in something[0]['role']:
                    return f(*args, **kwargs)
                else:
                    return jsonify(
                        status='Error',
                        msg="Sorry you do not have access to view this page "
                    )

            else:
                return jsonify(
                    status="Error",
                    msg="Kindly Login to continue"
                )
        return wrapped
    return wrapper


def requires_admin_old(*something):
    def wrapper(f):
        @wraps(f)

        def wrapped(*args, **kwargs):
            print("Session in wrapped: ", session)
            if "loggedIn" in session and session['loggedIn']:

                if 'username' in session:

                    username = session['username']
                    user = get_one("user", "username", username)

                    if user['type'] in something[0]['role']:
                        return f(*args, **kwargs)
                    else:
                        return jsonify(
                            status='Error',
                            msg="Sorry you do not have access to view this page"
                        )

            else:
                return jsonify(
                    status="Error",
                    msg="Kindly Login to continue"
                )
        return wrapped
    return wrapper




def is_admin(username):
    if username == 'admin':
        return True
    else:
        return False
