from flask import Blueprint, render_template, request, jsonify, session, Markup, send_from_directory
from utility.logger import logger
from datetime import datetime
from utility.generate_reports_json import generateReports
#from utility.generate_reports_excel import generateReportsWeekly
from utility.generate_repots_using_json import generateReportsWeekly
from utility.mongo_dao import *
from utility.de_coupled_report import *
import json
import os
metrics_blueprint = Blueprint('metrics', __name__, template_folder='templates')


# Return metrics of all bots
@metrics_blueprint.route('/api/metrics', methods=['GET'])
def metrics_all_bots():
    if request.method == 'GET':
        # Call the function with not bot_id passed
        returnedjson = generateReports()
        if returnedjson == 'Failure':
            return jsonify(
                status='Failure',
                metrics='Bot ID not found'
            )
        else:
            return jsonify(
                status='Success',
                metrics=returnedjson
            )
        # return returnedjson
        pass

# Return metrics of a particular bot
@metrics_blueprint.route('/api/metrics/<bot_id>', methods=['GET','POST'])
def metrics_bot(bot_id):
    if request.method == 'POST':
        #Call the function with bot_id passed
        try:
            pass
            # groups_accessible = user_json['group_list']
        except Exception as e:
            logger.exception(e)

        try:
            returnedjson = generateReports(bot_id)

            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    metrics='Bot ID not found'
                )
            else:
                return jsonify(
                    status='Success',
                    metrics=returnedjson
                )

        except Exception as e:
            return jsonify(
                status='Failure',
                message=e
            )

    if request.method == 'GET':
        #Call the function with bot_id passed
        try:
            returnedjson = generateReports(bot_id)

            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    metrics='Bot ID not found'
                )
            else:
                return jsonify(
                    status='Success',
                    metrics=returnedjson
                )

        except Exception as e:
            return jsonify(
                status='Failure',
                message=e
            )

'''
Should take a POST request input of:
1. user_id 
2. page number 
'''
@metrics_blueprint.route('/api/metrics/<bot_id>/convolog', methods=['GET', 'POST'])
def metrics_convolog_bot(bot_id):
    if request.method == 'POST':
        # Call the function with bot_id passed
        #todo - page_number = request.json['page_number']
        convo_log = get_ConvoLog()
        #todo - slice the returned JSON by the page number.
        return jsonify(convlog=convo_log)

# Return metrics of a particular bot
@metrics_blueprint.route('/api/metrics/<bot_id>/download', methods=['GET','POST'])
def metrics_download(bot_id):

    print("Metrics chinabot")
    try:
        start_date = datetime.strftime(datetime.today().replace(day=1), '%Y-%m-%d')
        end_date = datetime.strftime(datetime.today(), '%Y-%m-%d')
        print("Start date and end date: ", start_date, end_date)
        # start_date, end_date = start_end(start_date, end_date)
    except Exception as e:
        print(e)
    if request.method == 'GET':
        print('in get download')
        # try:
        filename1 = generateReportsWeekly(bot_id, start_date, end_date)
        file_name = str(bot_id) + '.xlsx'
        path = os.path.abspath("static/reports/")
        return send_from_directory(directory=path, filename=file_name)
        # except Exception as e:
        #     failure = 'Failure as ' + str(e)
        #     return failure
    if request.method == 'POST':
        print('in post download')
        #Call the function with bot_id passed

        try:
            filename1 = generateReportsWeekly(bot_id, start_date, end_date)
            logger.info(filename1)
            file_name = str(bot_id) + '.xlsx'


            #file_name = '_Chatbot_Report_Weekly_' + datetime.today().strftime('%m-%d-%Y') + '.xlsx'
            file_name = str(bot_id) + '.xlsx'
            path = os.path.abspath("static/reports/")
            print(file_name)
            return send_from_directory(directory=path, filename=file_name)
        except Exception as e:
            logger.exception(e)
            return jsonify(
                status='Failure',
                message=e
            )


@metrics_blueprint.route('/api/metrics/<bot_id>/convo', methods=['POST'])
def metrics_convo(bot_id):
    if request.method == 'POST':
        #Call the function with bot_id passed
        offset= request.json['offset']
        next = request.json['next']
        start = request.json.get("start_date") or  ""
        end = request.json.get("end_date") or ""
        try:
            grouplist = None
            bot_name = get_by_id('bot', bot_id).get('bot name')
            # groups_accessible = user_json['group_list']

        except Exception as e:
            logger.exception(e)
            print('user info read error - ', e)

        try:
            returnedjson,total = get_ConvoLog(start,end,bot_id, grouplist,offset,next)

            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    msg='Exception'
                )
            else:
                return jsonify(
                    status='Success',
                    metrics=returnedjson,
                    total=total,
                    bot_name=bot_name
                )

        except Exception as e:
            return jsonify(
                status='Failure',
                message=e
            )


@metrics_blueprint.route('/api/metrics/<bot_id>/intentfound', methods=['POST'])
def metrics_intentfound(bot_id):
    if request.method == 'POST':
        #Call the function with bot_id passed
        offset= request.json['offset']
        next = request.json['next']
        start = request.json.get("start_date") or  ""
        end = request.json.get("end_date") or ""
        try:
            grouplist = None
            bot_name = get_by_id('bot', bot_id).get('bot name')
            # groups_accessible = user_json['group_list']
        except Exception as e:
            logger.exception(e)
            print('user info read error - ', e)

        try:
            returnedjson,total = get_intent_found(start, end,bot_id, grouplist,offset,next)

            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    metrics='Exception'
                )
            else:
                return jsonify(
                    status='Success',
                    metrics=returnedjson,
                    total=total,
                    bot_name=bot_name
                )

        except Exception as e:
            return jsonify(
                status='Failure',
                message=e
            )

@metrics_blueprint.route('/api/metrics/<bot_id>/intentnotfound', methods=['POST'])
def metrics_intentnotfound(bot_id):
    if request.method == 'POST':
        #Call the function with bot_id passed
        offset= request.json['offset']
        next = request.json['next']
        start = request.json.get("start_date") or  ""
        end = request.json.get("end_date") or ""
        try:
            grouplist = None
            bot_name = get_by_id('bot', bot_id).get('bot name')
            # groups_accessible = user_json['group_list']
        except Exception as e:
            logger.exception(e)
            print('user info read error - ', e)

        try:
            returnedjson,total = get_intent_NotFound(start, end, bot_id, grouplist,offset,next)

            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    metrics='Exception'
                )
            else:
                return jsonify(
                    status='Success',
                    metrics=returnedjson,
                    total=total,
                    bot_name=bot_name

                )

        except Exception as e:
            return jsonify(
                status='Failure',
                message=e
            )

@metrics_blueprint.route('/api/metrics/<bot_id>/graph', methods=['POST'])
def metrics_graph_data(bot_id):
    if request.method == 'POST':

        try:

 
            grouplist = None
            grouplist  = request.json.get("group_name", [])
            bot_name = get_by_id('bot', bot_id).get('bot name')
        except Exception as e:
            logger.exception(e)
            print('user info read error - ', e)

        try:
            returnedjson = get_dashboard(bot_id,grouplist,bot_name)
            if returnedjson == 'Failure':

                return jsonify(
                    status='Failure',
                    metrics='Bot ID not found'
                )
            else:
                return jsonify(
                    status='Success',
                    bot_name=returnedjson['bot_name'],
                    graph_data=returnedjson['graph_data']


                )
        except Exception as e:
            return jsonify(
                status='Failure',
                message=e
            )
