from flask import Blueprint, request, jsonify, send_from_directory, send_file
import pandas as pd
from utility.decorators import requires_admin
from utility.mongo_dao import *
import xlsxwriter
adverse_event_blueprint = Blueprint('adverse_event', __name__, template_folder='templates')


# @adverse_event_blueprint.route('/api/upload_adverse_event_keywords', methods=['POST'])
# def upload_keys():
#     '''
#     This method takes file (.xlsx) as input and uploads the content in Adverse_Event collection
#     :return: success on successful execution
#     '''
#
#     try:
#         file = request.files['key_file']
#         print(file)
#     except:
#         return jsonify(
#             status='Failure',
#             msg='File upload error'
#         )
#     try:
#         xls = pd.ExcelFile(file)
#         sheetX = xls.parse(0)  # 2 is the sheet number
#         # print(sheetX)
#         var1 = list(sheetX)
#         print(type(var1))
#         if search_one('Adverse_Event', {'keyword': var1[0]}) is None:
#             insert('Adverse_Event', {'keyword': var1[0]})
#         var1 = list(set(sheetX[var1[0]]))
#         print(var1)
#         for i in var1:
#             if search_one('Adverse_Event', {'keyword': i}) is None:
#                 insert('Adverse_Event', {'keyword': i})
#         return jsonify(
#                 status='Success',
#                 msg="secret keys created successfully."
#             )
#     except Exception as e:
#         return jsonify(
#             status='Error',
#             msg="Error during key upload"
#         )


@adverse_event_blueprint.route('/api/download_adverse_event_keywords', methods=['GET', 'POST'])
@requires_admin({'role': ['admin']})
def download_AdverseKeywords():
    try:
        print("Inside try")
        path = os.path.abspath("static/reports/")
        print(path)
        fileTitle = os.path.join(path,'adverse_events.xlsx')
        print('filetitle',fileTitle)

        workbook = xlsxwriter.Workbook(filename=fileTitle)
        ae_keywords = workbook.add_worksheet('Adverse_Keywords')
        print(workbook)

        collection = get_collection('Adverse_Event')
        print(type(collection))
        print('collection is', collection)
        keywords=[]
        for cursor in collection:
            print("Each document: ", cursor)
            keywords.append(cursor['keyword'])
        print('keywords list is:',keywords)
        headers = ['Keywords']
        ae_keywords.write_row('A1', headers)

        for row,data in enumerate(keywords):
            print(keywords[row])
            row = row + 1
            ae_keywords.write(row, 0, data)

        workbook.close()
        print("Workbook closed")
        return send_file(fileTitle)

    except Exception as e:
        print(e)
        return jsonify(
            status = 'Error',
            msg = "Exception " + str(e),
        )



@adverse_event_blueprint.route('/api/upload_adverse_event_keywords', methods=['POST'])
@requires_admin({'role': ['admin']})
def upload_AdverseKeywords():
    try:
        message_request = request.get_json(force=True)['adverseObj']
        print("Message request received: ", message_request)
        keywords_list = [keyword["keyword"] for keyword in message_request]
        print("Keywords list:", keywords_list)
        for each_keyword in keywords_list:
            if (search_one('Adverse_Event', {"keyword": each_keyword}) is None) and each_keyword != "":
                insert('Adverse_Event', {"keyword": each_keyword})
                print("Inserted")
            else:
                return jsonify(
                    status='Error',
                    msg="No duplicate or blank values are allowed"
                )
        return jsonify(
            status="Success",
            msg="Keywords inserted successfully"
        )


    except Exception as e:
        return jsonify(
            status='Error',
            msg="Exception " + str(e)
        )





@adverse_event_blueprint.route('/api/add_adverse_event_keyword', methods=['POST'])
@requires_admin({'role':['admin']})
def add_AdverseKeyword():
    try:
        message_request = request.get_json(force=True)['adverseObj']
        print(message_request)
        keyword = message_request["keyword"]
        adverse_keyword = get_one("Adverse_Event", "keyword", keyword)
        if adverse_keyword is not None:
            return jsonify(
                status='Error',
                msg="Adverse Keyword already exists"

            )
        else:
            insert("Adverse_Event", {"keyword": keyword})
            return jsonify(
                status='Success',
                msg="Keyword inserted successfully"

            )
    except Exception as e:
        return jsonify(
            status='Error',
            msg="Exception " + str(e)

        )

@adverse_event_blueprint.route('/api/read_adverse_event_keywords', methods=['POST'])
@requires_admin({'role':['admin']})
def read_AdverseKeywords():
    adverse_keywords = get_collection('Adverse_Event')
    adverse_keywords_list = []
    for i in adverse_keywords:
        adverse_keywords_list.extend([{"_id": str(i["_id"]), "keyword": i["keyword"]}])

    return jsonify(
            status='Success',
            msg="Keys read successfully",
            Keywords=adverse_keywords_list
    )


@adverse_event_blueprint.route('/api/update_adverse_event_keywords', methods=['POST'])
@requires_admin({'role':['admin']})
def update_AdverseKeyword():
    try:
        message_request = request.get_json(force=True)['adverseObj']
        keyword_id = message_request['_id']
        kywd = message_request["keyword"]
        keyword = get_by_id("Adverse_Event", keyword_id)
        if keyword is None:
            return jsonify(
                status='Error',
                msg="Key with given id not available"

            )
        else:
            update("Adverse_Event", keyword_id, {"keyword": kywd})
            return jsonify(
                status='Success',
                msg="Keyword edited successfully"

            )
    except Exception as e:
        return jsonify(
            status='Error',
            msg="Exception " + str(e)

        )


@adverse_event_blueprint.route('/api/delete_adverse_event_keywords', methods=['POST'])
@requires_admin({'role':['admin']})
def delete_AdverseKeyword():
    try:
        adverse_event_request = request.get_json(force=True)['adverseObj']
        keyword_id = adverse_event_request["_id"]
        # result = get_by_id('Adverse_Event', keyword_id)
        # print(result)
        # keyword = result['keyword']
        # to_delete = search_one('besponsa_session_mapping', {'keyword': keyword})
        # print(to_delete)
        # if to_delete:
        #     delete("Adverse_Event", keyword_id)
        # else:
        #     delete("Adverse_Event", keyword_id)

        delete("Adverse_Event", keyword_id)
        return jsonify(status='Success', msg="Adverse keyword deleted successfully")

    except Exception as e:
        return jsonify(
            status='Error',
            msg=e
        )

